function [r1] = read_waterlevels_from_netcdf(start_year,n_year,no_cycles)
% read_waterlevels_from_netcdf.m (Joep Keijsers, 2014)
% read_waterlevels_from_netcdf.m reads waterlevels from the NetCDF file
% supplied bij Rijkswaterstaat, obtained from http://opendap.deltares.nl/thredds/dodsC/opendap/rijkswaterstaat/waterbase/sea_surface_height/id1-WESTTSLG.nc.html
% the temporal resolution changes over time, from 3-hourly values in the
% beginning of the timeseries to 10-min values is the last years.
% Values are extracted between the <start_year> and <end_year> with a frequency of
% <no_cycles> every year.

%% NetCDF description
% time: days since 1970-01-01 00:00:00 +01:00
% sea_surface_height: water height in cm with respect to normal amsterdam level in surface water

%% Uncomment the following lines to run stand-alone
 %start_year = 1991;       % first year of period
 %n_year = 9;        % y - length of period
 %no_cycles = 1;   % /y - number of cycles per year 

%% set directory containing NetCDF files 
ncfile  = './data/id1-WESTTSLG_2013.nc';

%% convert start_year to unix time 
start_time = datenum(start_year,5,1) - datenum(1970,1,1);
end_time = datenum(start_year+n_year,5,1) - datenum(1970,1,1);

%% Find exact start and end position of the timeseries
time = ncread(ncfile, 'time', 1, Inf, 1);

%% Find closest match in for start and end time
st = find(time<start_time,1,'last');
en = find(time>end_time,1,'first');

%% extract full series between the closest matches
sea  = ncread(ncfile,'sea_surface_height',[st,1], [en-st+1,1]);
time  = ncread(ncfile,'time',[st], [en-st+1]);

%% determine maximum per cycle (26 neap-tide cycles per year)
numBins = (n_year)*no_cycles; % define number of bins
binEdges = linspace(min(time), max(time), numBins+1);

[~,whichBin] = histc(time, binEdges);
binMax = nan(1,numBins);
for i = 1:numBins
    flagBinMembers = (whichBin == i);
    binMembers     = sea(flagBinMembers);
    binMax(i)     = max(binMembers);
end


%% plot results for checking
%x = linspace(start_year, start_year+n_year, length(binMax))
%figure;plot(x, binMax,'b*-')
%hold on
%mu = mean(binMax);

%hl = plot([start_year, start_year+n_year], [mu, mu])
%set(hl,'Color','r');
%legend('maximum','mean','location','northeast')

%% return array of maximum sea levels per cycle
clear sea time
r1 = binMax;

