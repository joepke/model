*****************
Folder structure:

* "/model/" folder contains .m files required for running the DUBEVEG and analysing its output

* "/model/data/" contains:
	- LIDAR measurements of beach and dune profiles, obtained from the JARKUS dataset, combined with vegetation cover data estimated from air photos (see Keijsers et al. paper in Geomorphology, 2015). One file for Ameland, one file for Terschelling
	- A time series of tide level data measured at West-Terschelling

* "/model/scenarios/" contains 50 unique scenarios of tide levels for the periods 2002-2050 and 2002-2100. Each has a sequence of tide levels generated from the statistical distributions of historical data.

* "/model/old/" contains old, unused .m and .mat files that might be useful someday

* "/model/temp/" is used to store model output, such as images and output-objects

*************
Matlab files:

* "run_model.m" : configure and execute a model run for years in the 1965-2012 period using observed tides, elevation and vegetation cover

* "run_model_scenario.m": configure and execute a model run using known initial conditions, but using synthetic tide data and vegetation/sea-level scenarios


