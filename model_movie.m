function [r1, r2, r3, r4]=model_movie(area, profile, startyear, simy, vegc, slabh, mht, hopl, wakelen, gwred, pdepb, pdeps, wavee, dissi, depthl, shadowa, reposeb, reposev, a1, b1, c1, d1, e1, p1, a2, b2, d2, e2, p2)
% Dune development model
% includes sand transport, vegetation growth of 2 species and beachupdate
% Andreas Baas, modified by Alma de Groot, modified by Joep Keijsers
% 28/02/2014
% simy=5;vegc=100;slabh=0.1;mht=2;hopl=1;wakelen=5;
%
% m-files and function files necessary for running:
% shadowzones2.m
% erosprobs.m
% depprobs.m
% shiftslabs3.m
% enforceslopes3.m
% beachupdatecacomnoran.m
% growthfunction1.m
% growthfunction2.m
% plotsforpres_rijtje.m

% parameters that need to be integer or rounded:
simy        = round(simy);
vegc        = round(vegc);
slabh       = round(slabh*100)/100;
mht         = round(mht*10)/10;
hopl        = round(hopl);
wakelen     = round(wakelen);
gwred       = round(gwred*10)/10;
if (a1<b1<c1<d1<e1 && a2<b2<d2<e2) == 0
    error('Growth function error: need increasing x-values')
end

%% TIME AND SPACE SETTINGS & LOADING INITIAL MAPS
simyears = simy ;        % number of years simulated
vegetationcycle = round(vegc/ (hopl/1) / (slabh/0.1) / (0.6/pdeps));   % cycle for updating vegetation response: this determines how long a year is [iterations/year]
beachreset   = round(0:(vegetationcycle/26):vegetationcycle*simy);        % how often to reset the wet beach part [iterarions] Should correspond to appr 2 weeks
time = 1:(simyears*vegetationcycle);
imagecycle = 10;
% topography settings 
% (already used for creating initial matrices to be loaded)
cellsize    = 1;                    % interpreted cell size [m]
slabheight  = slabh;                  % as a ratio of cell dimension
slabheightm = cellsize*slabheight;  % slab height in m

% read JARKUS profile from NetCDF file
[topoin, vegin] = read_profile_from_netcdf(area, profile,startyear,0);

% create required topographies and initial vegetation map
[topo, eqtopo, spec1at0, spec2at0] = create_topographies(topoin, vegin);

% equilibrium topography
% load          eqtopo01x250;                  % get equilibrium beach (= part of topo0) [m]
eqtopo      = round(eqtopo./slabheightm);     % transform from m into number of slabs
topo0       = round(topo./slabheightm); 
waterlevels = read_waterlevels_from_netcdf(startyear,simy,26);

% x and y coordinates
crossshore = size(topo0, 2).*cellsize;    % cross-shore size of topography [m]
longshore  = size(topo0, 1).*cellsize;    % along-shore size of topography [m]
x = (cellsize:cellsize:ceil(crossshore)); % shore normal axis for plotting. 
y = (cellsize:cellsize:ceil(longshore));  % shore parallel axis for plotting


%% MODEL PARAMETERS
jumplength  = round(hopl);         % hop length for slabs
MHT         = mht;      % offset with respect to zero level in model: assumption is that MHT = 0 m
maxvegeff   = 1.0;
p_dep_sand  = pdeps;       % [0,1] usually 0.6
p_dep_bare  = pdepb;       % [0,1] usually 0.4
shadowangle = shadowa;        % in degrees
repose_bare = reposeb;        % in degrees
repose_veg  = reposev;        % in degrees
repose_threshold = 0.4;  % vegetation threshold for applying repose_veg
gw_reduction   = gwred; 
grred = 0;
gw           = round(eqtopo .* gw_reduction);           % gw lies under beach with less steep angle
waterlevels  = (waterlevels + MHT)./slabheightm;  % [slabs]

%% STARTING CONDITIONS
topo         = topo0;               % initialise the topography map (slab units)
spec1        = spec1at0;       % initialise effectiveness map for species 1
spec2        = spec2at0;          % initialise effectiveness map for species 2 => 0
spec3        = spec1.*0;            % initialise effectiveness map for species 3 => 0
balance      = topo*0;              % initialise the sedimentation balance map [slabs]
%balancecopy  = balance;
inundated    = zeros(size(topo));   % initial area of wave/current action
pbeachupdatecum = zeros(size(topo));
beachcount   = 0;
%year=0;

veg = spec1+spec2+spec3;            % determine the initial cumulative vegetation effectiveness
veg(veg>maxvegeff)=maxvegeff;                       % cumulative vegetation effectiveness cannot be negative or larger than one
veg(veg<0)=0;                       


%% INITIALISING MATRICES FOR BUDGET CALCULATIONS
iterations = vegetationcycle*simyears;   % number of iterations
timeits = (1:1:iterations);              % time vector for budget calculations
seainput_slabs = zeros(size(timeits));   % inititalise vector for sea-transported slabs
windtransp_slabs = zeros(size(timeits)); % inititalise vector for wind-transported slabs

V = nan(1,iterations);
[vegs, topos] = deal(nan(crossshore,simy+1));

% initial dune volume
mtopo = mean(topo.*slabheight,1);
df = find(mtopo>3, 1, 'first');
z  = mtopo(df:end);
V0 = sum(z-3);

topos(:,1) = mean(topo0,1)*slabheight;
vegs(:,1) = mean(veg,1);


%% THE MAIN ITERATION LOOP 
% (1 loop = 1 iteration)
% X = 'start iteration';
% disp(X)
h = figure('visible','off');
i = 1;
dt = 1;
while i<=iterations        
    
%     X = sprintf('iteration %i of %i', i,iterations);    
    year = floor(i/vegetationcycle);
%     jumplength = randi(3);
%     dt = jumplength;


    %% SAND TRANSPORT
    sandmap       = topo>0;                                       % returns T(one) for sandy cells
    shadowmap     = shadowzones2(topo, slabheight, shadowangle);  % returns T(one) for in shadow
    
    erosmap       = erosprobs(veg, shadowmap, sandmap, topo, gw); % returns map of erosion probabilities
    deposmap      = depprobs(veg./4, shadowmap, sandmap, p_dep_bare, p_dep_sand); % returns map of deposition probabilities
    
    [changemap,slabtransp,pickedup] = shiftslabs3(erosmap, deposmap, jumplength);  % returns map of height changes
    %X = sprintf('picked up %i', pickedup);
%     disp(X)
    topo          = topo+changemap;                               % changes applied to the topography
    balance       = balance+changemap;                            % update the sedimentation balance map
    gw            = topo .* gw_reduction;    
    before        = topo;                                         % make copy for later use
    [topo, aval]  = enforceslopes3(topo, veg, slabheight, repose_bare, repose_veg, repose_threshold); % enforce angles of repose: avalanching
    windtransp_slabs(i) = slabtransp + aval;                      % transport by initial transport and avalanching together
    
        
    %% BEACHUPDATE
    if ismember(i,beachreset)                % update the inundated part of the beach
        inundatedold      = inundated;        % make a copy for later use
        before1           = topo;             % copy of topo before it is changed
        beachcount        = beachcount + 1;
        
        %function [r1,r2,r3] = beachupdatecacomnoran(sealevf, slabheightmf, cellsizef,...
        %       topof, eqtopof, vegf, m26f, m27af, m27bf, m28f, pwavemaxf, pwaveminf, depthlimitf, shelterf)
        
        m26f            = dissi; % parameter for dissipation strength ~[0.01 - 0.02]
        m27af           = wavee;    % wave energy factor
        m27bf           = 0;    % background wave energy
        m28f            = 0.0;  % resistance of vegetation: 1 = full, 0 = none. 
        pwavemaxf       = 1;    % maximum erosive strenght of waves (if >1: overrules vegetation)
        pwaveminf       = 0.0;  % in area with waves always potential for action
        depthlimitf     = depthl;  % (m) no erosive strength in very shallow water
        shelterf        = 1.0;  % exposure of sheltered cells: 0 = full shelter, 1 = no shelter.
       
        [topo, inundated, pbeachupdate] = beachupdatecacomnoran_test(waterlevels(beachcount), slabheightm, cellsize, topo, eqtopo ,veg , ...
            m26f, m27af , m27bf , m28f, pwavemaxf, pwaveminf , depthlimitf, shelterf);
        
        topo = round(topo);
        seainput          = topo - before1;   % sand added to the beach by the sea 
       % seainput_slabs(i) = nansum(seainput(:)); % total # of slabs added in this update
        seainput_slabs(i) = sum(seainput(~isnan(seainput)));
        
        inundated         = inundated|inundatedold;          % combine updated area from individual loops
        pbeachupdatecum   = pbeachupdate + pbeachupdatecum;  % cumulative beachupdate probabilities
        
%         figure(hh);    
%         surf(topo*slabheight); shading interp; axis equal;
%         title(['t = ', i/(simy*vegc)]);
  
    end
    [topo, aval2]         = enforceslopes3(topo, veg, slabheight, repose_bare, repose_veg, repose_threshold); % enforce angles of repose again
    aval                  = aval + aval2;      % combine the results of the two avalance actions
    balance               = balance + (topo - before);
    balancecopy           = balance;          % copy the balance map for plotting
    

    
    %% VEGETATION
    if mod(i,vegetationcycle) == 0
        
        spec1     = growthfunction1_sens(spec1, balance.*slabheightm, a1, b1, c1, d1, e1, p1);
        spec2     = growthfunction2_sens(spec2, balance.*slabheightm, a2, b2, d2, e2, p2);
        
        % only allow growth around existing patches      
%         spec2_gr     = growthfunction2_sens(spec2, balance.*slabheight, a2, b2, d2, e2, p2);
%         spec2_valid  = clonal_growth(spec2,1);
%         spec2        = spec2 .* spec2_valid;
        
%         spec1_gr     = growthfunction1_sens(spec1, balance.*slabheight, a1, b1, c1, d1, e1, p1);
%         spec1_valid  = clonal_growth(spec1,1);
%         spec1        = spec1_gr .* spec1_valid;

        % growth reduction due to salinity
        m4 = 75; 	    % higher values give same reduction at higher elevation
        m5 = 1; 		% lower values give slower transition 
        growthred_height = 1./(1 + m4.*exp(-m5.*topo.*slabheight)); % growth reduction function from the main nucom loop
        
        % no growth reduction landward of crest (sheltered from spray)
%         if (max(mean(topo.*slabheight))>6)
%             crest = find_crest(mean(topo.*slabheight,1), 6);
%             growthred_height(:,crest:end) = 1;
%         end
        
        spec1     = spec1.*growthred_height;
        spec2     = spec2.*growthred_height;
        grred     = 1;
        % end growth reduction
        
        pbeachupdatecum(pbeachupdatecum<0) = 0;
        pbeachupdatecum(pbeachupdatecum>1) = 1;
        spec1     = spec1.*(1-pbeachupdatecum);   % remove species where beach is reset
        spec2     = spec2.*(1-pbeachupdatecum);  
        %spec2 = 0;
        spec1_geom          = spec1;     % limit to geomorphological range
        spec1_geom(spec1<0) = 0;
        spec1_geom(spec1>1) = 1;
        spec2_geom          = spec2;
        spec2_geom(spec2<0) = 0;
        spec2_geom(spec2>1) = 1;
        veg                 = spec1_geom + spec2_geom;      % update vegmap
        veg(veg>maxvegeff)  = maxvegeff;        % limit to effective range
        veg(veg<0)          = 0;     
        balance(:)          = 0;        % reset the balance map
        inundated(:)        = 0;        % reset part of beach with wave/current action
        pbeachupdatecum(:)  = 0;
    end
    
    if mod(i,vegetationcycle) == 0
        topos(:,year+1) = mean(topo,1)*slabheight;
        vegs(:,year+1) = mean(spec1_geom+spec2_geom,1);
        
        
        veg = add_wakes(veg,wakelen);
        veg(veg>maxvegeff)  = maxvegeff;        % limit to effective range
        veg(veg<0)          = 0;     

    
%         h=figure('visible','on');
%         hold on
%         set(gca,'ColorOrder',jet(size(topos,2)))
% 
%         subplot(2,1,1)
%         set(gca,'ColorOrder',jet(size(topos,2)))
%                 hold on
%         set(gca,'ColorOrder',jet(size(topos,2)))
%         plot(topos)
%         
%         subplot(2,1,2)
%                 hold on
%         set(gca,'ColorOrder',jet(size(topos,2)))
%         plot(vegs)
% %         
%         save_fig(h, ['D:/Dropbox/model/fig/sims/c18_prof_',num2str(profile),'_2000_2010_',num2str(vegetationcycle),'_',num2str(mht),'.tif'], 6,6,300,0)
    end
    

    
    
%     %     % only topography
    if mod(i,imagecycle)==0         % update the images
        if (exist('endtopo','var'))
            bal = topo-endtopo;
        else
            bal = zeros(size(topo));
        end
        s1 = spec1;
        s2 = spec2;
        s1(spec1<0) = 0;
        s1(spec1>1) = 1;
        s2(spec2<0) = 0;
        s2(spec2>1) = 1;
        veg = s1 + s2;
        
        figure(h);
        surf(x,y,topo.*slabheight); shading interp; axis equal; zlim([0 20]); title(['t = ',num2str(i/vegetationcycle),' y' ]);
        hold on
        xx = mean(pbeachupdate,1);
        xx = find(xx==0,1,'first');
        plot3([xx xx], [0 max(y)], [4 4])
%         a = summer;
%         colormap(a(end:-1:1,:))
%         R = [linspace(0,1,24),0,0,zeros(1,24)];
%         G = [zeros(1,24),0,0,linspace(0,1,24)];
%         B = linspace(0,0,50);

%         T = [R', G', B'];

%         map(1:50, :) = T;
%         colormap(map)
        filmpje(i/10) = getframe(h);
        close;  
        endtopo = topo;
    end
% %  
% %  in case a movie needs to be made, make the following line active

    
    %% CALCULATE STATE VARIABLES
    % dune volume    
    mtopo = mean(topo.*slabheight,1);
    df = find(mtopo>3, 1, 'first');
    z  = mtopo(df:end);
    V(i) = sum(z-3);
    
    i = i + dt;

end

% calculations of model performance
% figure;
% hold on
% set(gca,'ColorOrder',jet(size(vegs,2)))
% plot(vegs)
% legend('Location','northeast')


% measured volume (JARKUS)
% area=3;profile=17000;startyear=2002;simyears=9;
[jarkus, veg] = read_profile_from_netcdf(area,profile,startyear,simyears);

V_jar = nan(1,size(jarkus,2));
for i = 1:size(jarkus,2)
    if ~all(isnan(jarkus(:,i)))
        df = find(jarkus(:,i)>3, 1, 'first');
        z  = jarkus(df:end,i);
        if all(~isnan(z))
            V_jar(i) = sum(z-3);
        end
    end
end

V_mod = [V0, V(vegetationcycle:vegetationcycle:simyears*vegetationcycle)];
% V_mod = V_mod-V_jar(1);
% V     = V - V_jar(1);
% V_jar = V_jar-V_jar(1);

% V_mod = V_mod(~isnan(V_jar));
% V_jar = V_jar(~isnan(V_jar));
dV_mod = diff(V_mod);
dV_jar = diff(V_jar);

% quality of change in volume
dV_mod = dV_mod(~isnan(dV_jar));
dV_jar = dV_jar(~isnan(dV_jar));
NSEdvol       = 1 - (sum( (dV_jar - dV_mod).^2) / sum( (dV_jar - mean(dV_jar)).^2));

V_mod2 = V_mod(~isnan(V_jar));
V_jar2 = V_jar(~isnan(V_jar));

% NSEvol       = 1 - (sum( (dV_jar - dV_mod).^2) / sum( (dV_jar - mean(dV_jar)).^2));
NSEvol       = 1 - (sum( (V_jar2 - V_mod2).^2) / sum( (V_jar2 - mean(V_jar2)).^2));

% changes in elevation
dz_model  = topos(:,size(topos,2)) - topos(:,1);
dz_jarkus = jarkus(:,size(jarkus,2)) - jarkus(:,1);

% only non-NA values
dz_mod = dz_model(~isnan(dz_jarkus));
dz_jar = dz_jarkus(~isnan(dz_jarkus));

% downsample to original JARKUS resolution (1m to 5m)
dz_mod = dz_mod(1:5:end);
dz_jar = dz_jar(1:5:end);

% quality of change in morphology
% calculate Nash-Sutcliffe efficiency
NSEprof       = 1 - (sum( (dz_jar - dz_mod).^2) / sum( (dz_jar - mean(dz_jar)).^2));

if sum(startyear+simy == [ 2003 2006 2009 2011]) > 0 
    % quality of vegetation cover in simulation
    veg_mod   = spec1_geom + spec2_geom;
    veg_mod(veg_mod>1) = 1;
    veg_mod(veg_mod<0) = 0;
    veg_mod   = mean(veg_mod,1);
    veg_obs   = transpose(veg(:,end));
    
    % calculate Nash-Sutcliffe efficiency
    NSEveg       = 1 - (sum( (veg_obs - veg_mod).^2) / sum( (veg_obs - mean(veg_obs)).^2));
    
else
    NSEveg = nan;
end

%% MAKE FIGURES AND MOVIES
% endplots
r1 = NSEvol;
r2 = NSEprof;
r3 = NSEveg;
r4 = filmpje;
filmpjewegschrijven
%r4 = topo;
%r5 = spec1+spec2;
%r6 = veg_mod;
%r7 = topos;
% movie(filmpje)
