function [r1] = establish_new_vegetation(topof, mht, prob)

% [topo, veg] = read_profile_from_netcdf(3, 20000, 2002,0);
% [topof, eqtopo, vegf, veg2] = create_topographies(topo, veg);
% mht = 0;
% convert topography to height above current sea level:
topof = topof - mht;

% vertices (these need to be specified)
x1 = 0.0;        y1 = prob;  
x2 = 2.0;        y2 = prob;
x3 = 3.0;        y3 = prob; 
x4 = 4.5;        y4 = prob;
x5 = 20.0;       y5 = prob;

% slopes between vertices (calculated from vertices)
s12 = (y2-y1)/(x2-x1);
s23 = (y3-y2)/(x3-x2);
s34 = (y4-y3)/(x4-x3);
s45 = (y5-y4)/(x5-x4);

leftextension=bsxfun(@lt, topof, x1)                   .* 0;
firstleg =bsxfun(@ge, topof, x1).*bsxfun(@lt, topof, x2) .* ( (topof-x1)*s12 + y1);
secondleg=bsxfun(@ge, topof, x2).*bsxfun(@lt, topof, x3) .* ( (topof-x2)*s23 + y2);
thirdleg =bsxfun(@ge, topof, x3).*bsxfun(@lt, topof, x4) .* ( (topof-x3)*s34 + y3);
fourthleg=bsxfun(@ge, topof, x4).*bsxfun(@lt, topof, x5) .* ( (topof-x4)*s45 + y4);
rightextension=bsxfun(@ge, topof, x5)                  .* 0;

pioneer_establish_prob = leftextension+firstleg+secondleg+thirdleg+fourthleg+rightextension;
pioneer_established = bsxfun(@lt, rand(size(topof)), pioneer_establish_prob); 

r1 = pioneer_established;
% r1=vegf+leftextension+firstleg+secondleg+thirdleg+fourthleg+rightextension;

% subplot(1,2,1)
% imagesc(pioneer_establish_prob)
% subplot(1,2,2)
% imagesc(pioneer_established)




