%% run dune simulation model (model.m) with parameters specified below
% profile on
clear all; close all;
isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0;
if isOctave
	pkg load netcdf statistics
end

% set seed for random number generator 
rand('state', 1234)
% parameters
area_code = 3;
start_year = 2002;
simulation_time_y = 9;
profile_id = 20000;

   
% read JARKUS profile from NetCDF file
[topoin, vegin] = read_profile_from_netcdf(area_code, profile_id, start_year, 0);

% get default model input data:
OPT = model_input;

%% Model run configuration
OPT.zInitial =  topoin';
OPT.zInitial =  vegin';
OPT.area_code =  area_code;
OPT.profile_id = profile_id;
OPT.start_year = start_year;
OPT.simulation_time_y = simulation_time_y;

%% Modify settings if necessary: 
for r = 1
    rand('state', r)

    set_value(OPT, 'repose_bare', 30);
    set_value(OPT, 'repose_vegetated', 35);
    set_value(OPT, 'p_deposition_bare', 0.13);
    set_value(OPT, 'p_deposition_veg', 0.3);
    set_value(OPT, 'iterations_per_cycle', 52);
    set_value(OPT, 'sp1_b', 0.05);
    set_value(OPT, 'depth_limit', 0.4);
    set_value(OPT, 'dissipation_rate', 0.013);

    % run simulations and get output results
    [OUT] = model_sens(OPT);

%     fn = ['./temp/calibrated_model_pr', num2str(profile_id), '_a', num2str(area_code),'_r', num2str(r), '_25m.mat'] ;
    disp([num2str(profile_id),  ', dz: ', num2str(OUT.ME_dz), ', V: ', num2str(OUT.ME_V), ', C: ', num2str(OUT.ME_C)])
                fn = './temp/no_stability_crit.mat'
    save(fn, 'OUT');
end
    
% profile off
% profile viewer