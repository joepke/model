function [] = write_movie(fn, M)
%% write_movie(fn, M)
% fn    : intended filename
% M     : collection of frames

% mov = avifile(fn, 'fps', 5, 'quality',100, 'compression','none');  
mov = VideoWriter([fn,'.avi'], 'Archival');
mov.FrameRate = 5;
% fps was 10
% movie file is now created and opened
% making it at slow-motion
% change name at beginning to avoid overwriting

%% settings
startFrame=1;
endFrame=size(M,2);  % should be number of frames in Matlab movie (see workspace)

% open movie object
open(mov);
%% fill movie with recorded frame
for frameNumber=startFrame:endFrame
%     mov = addframe(mov,M(frameNumber));
    writeVideo(mov,M(frameNumber))
%     close
end

close(mov);  % movie file is closed again