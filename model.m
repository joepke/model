function [r1, r2, r3, r4, r5, r6]=model(startyear, simy, vegc, slabh, mht, hopl, wakelen, profile, waketype)
% Dune development model
% includes sand transport, vegetation growth of 2 species and beachupdate
% Andreas Baas, modified by Alma de Groot, modified by Joep Keijsers
% 28/02/2014
% simy=5;vegc=100;slabh=0.1;mht=2;hopl=1;wakelen=5;
%
% m-files and function files necessary for running:
% shadowzones2.m
% erosprobs.m
% depprobs.m
% shiftslabs3.m
% enforceslopes3.m
% beachupdatecacomnoran.m
% growthfunction1.m
% growthfunction2.m
% plotsforpres_rijtje.m

if nargin < 3 || isempty(vegc)
   vegc = 100;
end

if nargin < 3 || isempty(slabh)
   slabh = 0.1;
end

%close all   % closes all open figures
%clear all   % clears all variables from the workspace

%% TIME AND SPACE SETTINGS & LOADING INITIAL MAPS
simyears = simy;         % number of years simulated
vegetationcycle = vegc;   % cycle for updating vegetation response: this determines how long a year is [iterations/year]
time = 1:(simyears*vegetationcycle);
imagecycle = vegc;        % how often to update the image (in iterations)
beachreset   = round(0:(vegc/26):vegc*simy);        % how often to reset the wet beach part [iterarions] Should correspond to appr 2 weeks


% topography settings 
% (already used for creating initial matrices to be loaded)
cellsize    = 1;                    % interpreted cell size [m]
slabheight  = slabh;                  % as a ratio of cell dimension
slabheightm = cellsize*slabheight;  % slab height in m


% load initial maps from the workspace
% load          topo01x250;                    % also change in next line; starting topography [m]
% topo0       = topo01x250(1:50,:)./slabheightm;       % transform from m into number of slabs [slabs]
% load('topoameland.mat','topo','eqtopo');
% topo0 = topo./slabheightm;

% read JARKUS profile from NetCDF file
[topoin, vegin] = read_profile_from_netcdf(profile,startyear,0);

% create required topographies and initial vegetation map
[topo, eqtopo, spec1at0, spec2at0] = create_topographies(topoin, vegin);

% equilibrium topography
% load          eqtopo01x250;                  % get equilibrium beach (= part of topo0) [m]
eqtopo      = round(eqtopo./slabheightm);     % transform from m into number of slabs
topo0       = round(topo./slabheightm); 

% waterlevels
waterlevels = read_waterlevels_from_netcdf(startyear,startyear+simy,26);
%  load          hightide2wk19912010mMHTrep     % waterlevel series for beachupdate waterlevels [m + MHT Terschelling NZ]
% load waterlevels_2000_2010
%  waterlevels = hightide2wk19912010mMHTrep;    % (will be converted into slabs below)
% waterlevels = wl;
% waterlevels = rand(1,500)+0.75;

% initial vegetation
% load spec1at01x250;                         % initial vegetation, see workspace, also change in line 72
% spec1at01x250 = spec1at01x250(1:50,:);
% load spec2at0;
% load spec3at0;

% x and y coordinates
crossshore = size(topo0, 2).*cellsize;    % cross-shore size of topography [m]
longshore  = size(topo0, 1).*cellsize;    % along-shore size of topography [m]
x = (cellsize:cellsize:ceil(crossshore)); % shore normal axis for plotting. 
y = (cellsize:cellsize:ceil(longshore));  % shore parallel axis for plotting


%% MODEL PARAMETERS
jumplength  = hopl;         % hop length for slabs
MHT         = mht;      % offset with respect to zero level in model: assumption is that MHT = 0 m
maxvegeff   = 1.0;
extrawater  = 0; % in m, simulate higher water levels (jk)
p_dep_sand  = 0.6;       % [0,1] usually 0.6
p_dep_bare  = 0.4;       % [0,1] usually 0.4
shadowangle = 15;        % in degrees
repose_bare = 30;        % in degrees
repose_veg  = 40;        % in degrees
repose_threshold = 0.3;  % vegetation threshold for applying repose_veg
gw_reduction   = 0.8; 

gw           = round(eqtopo .* gw_reduction);           % gw lies under beach with less steep angle
waterlevels  = (waterlevels + MHT + extrawater)./slabheightm;  % [slabs]

%% STARTING CONDITIONS
topo         = topo0;               % initialise the topography map (slab units)
spec1        = spec1at0;       % initialise effectiveness map for species 1
spec2        = spec2at0.*0;           % initialise effectiveness map for species 2 => 0
spec3        = spec1.*0;            % initialise effectiveness map for species 3 => 0
balance      = topo*0;              % initialise the sedimentation balance map [slabs]
%balancecopy  = balance;
inundated    = zeros(size(topo));   % initial area of wave/current action
pbeachupdatecum = zeros(size(topo));
beachcount   = 0;
%year=0;

veg = spec1+spec2+spec3;            % determine the initial cumulative vegetation effectiveness
veg(veg>1)=1;                       % cumulative vegetation effectiveness cannot be negative or larger than one
veg(veg<0)=0;                       


%% INITIALISING MATRICES FOR BUDGET CALCULATIONS
iterations = vegetationcycle*simyears;   % number of iterations
timeits = (1:1:iterations);              % time vector for budget calculations
seainput_slabs = zeros(size(timeits));   % inititalise vector for sea-transported slabs
windtransp_slabs = zeros(size(timeits)); % inititalise vector for wind-transported slabs

W = nan(1,iterations);
V = nan(1,iterations);
topos = nan(crossshore,simy+1);

% initial dune volume
topo3 = topo.*slabheight >= 3;
topovol = topo3.*topo.*slabheight;
V0 = sum(topovol(:))/longshore;
    
%% FIGURE PLOTTING & IMAGING SETTINGS
% hh = figure;
h = figure('Visible','off'); 
% set(gcf,'Visible','off');
subplot(2,2,1); image(x,y,topo0.*slabheightm.*10); title ('topography'); % display the starting topography
axis equal tight; 
subplot(2,2,2); imagesc(x,y,spec1); title ('pioneer vegetation');
axis equal tight; 
subplot(2,2,3); plot(1:crossshore, mean(topo,1)*slabheight);
axis tight; 
subplot(2,2,4);


% k = figure;
% image(x,y,topo*amptopo + offtopo); title ('topography'); % display the starting topography
% axis equal tight; colorbar;
topos(:,1) = mean(topo0,1)*slabheight;


%% THE MAIN ITERATION LOOP 
% (1 loop = 1 iteration)

for i=1:iterations                 
    X = sprintf('iteration %i of %i', i,iterations);
    disp(X)
    year = floor(i/vegetationcycle);
    
    %% SAND TRANSPORT
    sandmap       = topo>0;                                       % returns T(one) for sandy cells
    shadowmap     = shadowzones2(topo, slabheight, shadowangle);  % returns T(one) for in shadow
    
    erosmap       = erosprobs(veg, shadowmap, sandmap, topo, gw); % returns map of erosion probabilities
    deposmap      = depprobs(veg, shadowmap, sandmap, p_dep_bare, p_dep_sand); % returns map of deposition probabilities
    
    [changemap,slabtransp,pickedup] = shiftslabs3(erosmap, deposmap, jumplength);  % returns map of height changes
    %X = sprintf('picked up %i', pickedup);
%     disp(X)
    topo          = topo+changemap;                               % changes applied to the topography
    balance       = balance+changemap;                            % update the sedimentation balance map
    gw            = topo .* gw_reduction;    
    before        = topo;                                         % make copy for later use
    [topo, aval]  = enforceslopes3(topo, veg, slabheight, repose_bare, repose_veg, repose_threshold); % enforce angles of repose: avalanching
    windtransp_slabs(i) = slabtransp + aval;                      % transport by initial transport and avalanching together
    
        
    %% BEACHUPDATE
    if ismember(i,beachreset)                % update the inundated part of the beach
        inundatedold      = inundated;        % make a copy for later use
        before1           = topo;             % copy of topo before it is changed
        beachcount        = beachcount + 1;
        
        %function [r1,r2,r3] = beachupdatecacomnoran(sealevf, slabheightmf, cellsizef,...
        %       topof, eqtopof, vegf, m26f, m27af, m27bf, m28f, pwavemaxf, pwaveminf, depthlimitf, shelterf)
        
        m26f            = 0.01; % parameter for dissipation strength ~[0.01 - 0.02]
        m27af           = 3;    % wave energy factor
        m27bf           = 0;    % background wave energy
        m28f            = 0.0;  % resistance of vegetation: 1 = full, 0 = none. 
        pwavemaxf       = 1;    % maximum erosive strenght of waves (if >1: overrules vegetation)
        pwaveminf       = 0.0;  % in area with waves always potential for action
        depthlimitf     = 0.5;  % (m) no erosive strength in very shallow water
        shelterf        = 0.0;  % exposure of sheltered cells: 0 = full shelter, 1 = no shelter.
       
        [topo, inundated, pbeachupdate] = beachupdatecacomnoran(waterlevels(beachcount), slabheightm, cellsize, topo, eqtopo ,veg , ...
            m26f, m27af , m27bf , m28f, pwavemaxf, pwaveminf , depthlimitf, shelterf);
        
        topo = round(topo);
        seainput          = topo - before1;   % sand added to the beach by the sea 
       % seainput_slabs(i) = nansum(seainput(:)); % total # of slabs added in this update
        seainput_slabs(i) = sum(seainput(~isnan(seainput)));
        
        inundated         = inundated|inundatedold;          % combine updated area from individual loops
        pbeachupdatecum   = pbeachupdate + pbeachupdatecum;  % cumulative beachupdate probabilities
        
%         figure(hh);    
%         surf(topo*slabheight); shading interp; axis equal;
%         title(['t = ', i/(simy*vegc)]);
  
    end
    [topo, aval2]         = enforceslopes3(topo, veg, slabheight, repose_bare, repose_veg, repose_threshold); % enforce angles of repose again
    aval                  = aval + aval2;      % combine the results of the two avalance actions
    balance               = balance + (topo - before);
    balancecopy           = balance;          % copy the balance map for plotting
    

    
    %% VEGETATION
    if mod(i,vegetationcycle) == 0
        
        topos(:,year+1) = mean(topo,1)*slabheight;
        spec1     = growthfunction1(spec1, balance*slabheightm);
        
        % growth reduction due to salinity
        m4 = 75; 	% parameter for calculating logistic growthreduction by salinity (elevation) (in m instead of slabheight)
        m5 = 2; 		% idem
        growthred_height = 1./(1 + m4.*exp(-m5.*topo0.*slabheight)); % growth reduction function from the main nucom loop
%         spec1     = spec1.*growthred_height;
%         spec2     = spec2.*growthred_height;
        % end growth reduction
        
%         spec2     = growthfunction2(spec2, balance*slabheightm);
        pbeachupdatecum(pbeachupdatecum<0) = 0;
        pbeachupdatecum(pbeachupdatecum>1) = 1;
        spec1     = spec1.*(1-pbeachupdatecum);   % remove species where beach is reset
        spec2     = spec2.*(1-pbeachupdatecum);  
        %spec2 = 0;
        spec1_geom          = spec1;     % limit to geomorphological range
        spec1_geom(spec1<0) = 0;
        spec1_geom(spec1>1) = 1;
        spec2_geom          = spec2;
        spec2_geom(spec2<0) = 0;
        spec2_geom(spec2>1) = 1;
        veg                 = spec1_geom + spec2_geom;      % update vegmap
        veg(veg>maxvegeff)  = maxvegeff;        % limit to effective range
        veg(veg<0)          = 0;     
        balance(:)          = 0;        % reset the balance map
        inundated(:)        = 0;        % reset part of beach with wave/current action
        pbeachupdatecum(:)  = 0;
        
        veg = add_wakes(veg,wakelen, waketype);
        veg(veg>maxvegeff)  = maxvegeff;        % limit to effective range
        veg(veg<0)          = 0;     

    end
    
    
    %% STATE VARIABLES
    % dune volume
    topo3 = topo.*slabheight >= 3;
    topovol = topo3.*topo.*slabheight;
    V(i) = sum(topovol(:))/longshore;
    
    % beach width
    profslope = diff(mean(topo*slabheight,1));
    if max(profslope) > 0.25
        W(i) = find(diff(mean(topo*slabheight,1))>0.25,1,'first');
    end;
    

%% FIGURES
% 4 images in same panel
    if mod(i,imagecycle)==0         % update the images
        
        figure(h);
        set(gcf,'Visible','off');
        
        subplot(2,2,1); image(x,y,topo.*slabheightm.*10); title(['topography at t = ',num2str(year),' years']);
        axis equal tight; %colorbar;
        hold on;
        plot(W(i),10,'r*');
        hold off;

        subplot(2,2,2); imagesc(x,y,(~(spec1==0)*1+~(spec2==0)*2));title ('pioneer vegetation');
        axis equal tight; %colorbar;

        subplot(2,2,3); plot(mean(topo,1)*slabheight);
        axis tight; ylim([0 15]);
        hold on
        plot(mean(topo0,1)*slabheight,'-k');
        plot(mean(veg,1)*10,'r');
        hold off

        subplot(2,2,4); plot((1:iterations)/vegetationcycle,V); title ('dune volume (m3/m)'); xlabel('year');
        %ylim([100 max(V(~isnan(V)))+50]);
        axis tight;

        % linear regression of dune volume
        p = polyfit(time(1:i)/vegetationcycle,V(1:i),1);
        r = p(1) .* time(1:i)/vegetationcycle + p(2); % compute a new vector r that has matching datapoints in x
        txt = [num2str(p(1),2),' m3/m/y'];
        a = axis;
        wdth = a(2)-a(1);
        ht = a(4)-a(3);
        pos = [a(1)+0.05*wdth a(4)-0.05*ht];
        text(pos(1),pos(2),txt);
        hold on;
        plot(time(1:i)/vegetationcycle, r, 'r-');
        hold off;
        
%         pause

    end 
end
    



%% CALCULATE SAND INPUT BY SEA
% values are converted from slabs to m3
seainput_m2 = seainput_slabs.*slabheightm.*(cellsize^2)./(longshore.*crossshore);  % flux over seaward boundary [m3/m = m2] and averaged over cross-shore length
timeyear = (1:1:simyears);                  % for each simulation year
seainputperyear_m2 = NaN(size(timeyear));   % initialise vector
windtranspyear_m2 = NaN(size(timeyear));    % initialise vector
for a = 1:simyears
    %     sum input per year for water and wind transport
    seainputperyear_m2(a) = ...
        sum(seainput_m2(1+(a-1)*vegetationcycle : vegetationcycle+(a-1)*vegetationcycle));
    windtranspyear_m2(a) = ...
        sum(windtransp_slabs (1+(a-1)*vegetationcycle : vegetationcycle+(a-1)*vegetationcycle))...
        .*slabheightm.*(cellsize^2)./(longshore.*crossshore);   
    % = transported volume times slab volume divided by width of topo
end


%% determine simulation success:
% trend in volume
V_mod = V-V0;
V_mod = [0 , V_mod(vegetationcycle:vegetationcycle:simyears*vegetationcycle)];

% measured volume (JARKUS)
jarkus = read_profile_from_netcdf(profile,startyear,simyears);
V_jar =  jarkus >= 3;
V_jar = V_jar.*(jarkus-3);
V_jar = sum(V_jar,1);
V_jar = V_jar .* all( ~isnan( jarkus ), 1 );
V_jar = V_jar - V_jar(1);

V_mod = V_mod(~isnan(V_jar));
V_jar = V_jar(~isnan(V_jar));
success_V = 1 - (rms(V_mod - V_jar)/mean(V_jar));

% changes in elevation
dz_model  = topos(:,size(topos,2)) - topos(:,1);
dz_jarkus = jarkus(:,size(jarkus,2)) - jarkus(:,1);

% only non-NA values
dz_mod = dz_model(~isnan(dz_jarkus));
dz_jar = dz_jarkus(~isnan(dz_jarkus));

% downsample to original JARKUS resolution (1m to 5m)
dz_mod = dz_mod(1:5:end);
dz_jar = dz_jar(1:5:end);

corr_dz = corr(dz_mod,dz_jar);
success_z = corr_dz;

% weighted success:
success_tot = (success_z + success_V)/2;

%% MAKE FIGURES AND MOVIES
% name = sprintf('D:/model/doc/fig/mht%4.2f_slabh%4.2fvegc%i', MHT, slabh, vegc);
% name = strrep(name,'.','_');
endplots

r1 = topos;
r2 = V;
r3 = spec1;
r4 = p(1);
r5 = topo*slabheight;
r6 = success_tot;
