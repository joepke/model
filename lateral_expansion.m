function [r1] = lateral_expansion(veg, dist, prob)
%LATERAL_EXPANSION implements lateral expansion of existing vegetation patches
% 1) mark cells that lie within <dist> of existing patches: probability
% for new vegetated cells = 1
% 2) cells not adjacent to existing patches get probability depending on
% elevation: pioneer most likely to establish between 3 and 5 m + sea
% level
%
%% for testing purposes:
% rand('state', 1234)
% veg = randi(20,10,10);
% veg = veg>19;
% dist = 1;
% prob = 0.1;

%% pad vegetation matrix with zeros for circshift
veg = veg>0;    
vegpad = zeros(size(veg)+2);
vegpad(2:end-1,2:end-1) = veg;
veg3 = vegpad;

% add shifted matrices to initial matrix to include boundaries
for i = [-dist 0 dist]
    for j = [-dist 0 dist]
        % Only 4 neighbouring cells (Von Neumann neighbourhood)
%         if mod(i*3+5+j,2)== 0
            veg2 = circshift(vegpad,[i j]);
            veg3  = veg3+veg2;
%         end
    end
end

veg3(veg3>1) = 1;
lateral_expansion_possible = veg3(2:end-1, 2:end-1);

% lateral expansion only takes place in a fraction <prob> of the possible
% cells
lateral_expansion_allowed = bsxfun(@lt, rand(size(veg)), lateral_expansion_possible .* prob); 
% include existing vegetation to incorporate growth or decay of existing
% patches
lateral_expansion_allowed = lateral_expansion_allowed + veg;
lateral_expansion_allowed = lateral_expansion_allowed > 0;

r1 = lateral_expansion_allowed;

%% test plots
% figure;
% subplot(1,3,1)
% imagesc(veg)
% subplot(1,3,2)
% imagesc(lateral_expansion_possible + veg)
% subplot(1,3,3)
% imagesc(lateral_expansion_allowed + veg)
% colormap(jet(3));
% caxis([0 2])
% lcolorbar({'Bare','Old veg.','New veg.'})

