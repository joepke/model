function [r1] = get_vegetation_cover(topo, veg, reference_level)
%GET_PROFILE_VOLUME calculates the sand volume stored in a dune above a
%reference level. 
%% uncomment to test
% reference_level = 3;
% [topo, veg] = read_profile_from_netcdf(3,20000,2011,0);


%% determine volume above reference level with trapezoidal rule
z = mean(topo,1);

if max(z) < reference_level
    warning('DUBEVEG:dunefoot_level_error','No cells above dune-foot level.')
    C = NaN;
else
    c1 = topo >= reference_level;
    c2 = veg > 0;
    C  = sum(c1 & c2) / sum(c1);
end

r1 = C;
