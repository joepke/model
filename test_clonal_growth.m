
slabheight = 0.1;
shadowangle = 15;
p_dep_bare = 0.4;
p_dep_sand = 0.6;
jumplength = 1;

[topo, veg] = read_profile_from_netcdf(20000,2002,0);
[topo, eqtopo, spec1, spec2] = create_topographies(topo, veg);

topo = topo./slabheight;
gw = 0.6.*topo;
veg = spec1 + spec2;
balance      = topo*0;              % initialise the sedimentation balance map [slabs]
 %% SAND TRANSPORT
for i = 1:20
    sandmap       = topo>0;                                       % returns T(one) for sandy cells
    shadowmap     = shadowzones2(topo, slabheight, shadowangle);  % returns T(one) for in shadow

    erosmap       = erosprobs(veg, shadowmap, sandmap, topo, gw); % returns map of erosion probabilities
    deposmap      = depprobs(veg, shadowmap, sandmap, p_dep_bare, p_dep_sand); % returns map of deposition probabilities

    [changemap,slabtransp,pickedup] = shiftslabs3(erosmap, deposmap, jumplength);  % returns map of height changes
    %X = sprintf('picked up %i', pickedup);
    %     disp(X)
    topo          = topo+changemap;                               % changes applied to the topography
    balance       = balance+changemap;                            % update the sedimentation balance map
end


% spec2_old    = spec2;
% spec2_gr     = growthfunction2(spec2, balance.*slabheight);
% spec2_valid  = clonal_growth(spec2_old,1);
% spec2_gr     = spec2_gr .* spec2_valid;
% spec2_new    = spec2_gr;

spec2_gr     = growthfunction2(spec2, balance.*slabheight);
spec2_valid  = clonal_growth(spec2,1);
spec2_new        = spec2_gr .* spec2_valid;

spec2_new2     = growthfunction2(spec2, balance.*slabheight);
spec2_new2     = spec2_new2;

figure;
subplot(4,1,1);
imagesc(spec2);
subplot(4,1,2);
imagesc(spec2_valid)
subplot(4,1,3);
imagesc(spec2_gr)
subplot(4,1,4);
imagesc(spec2_new-spec2)