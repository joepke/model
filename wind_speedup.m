function [r1]=wind_speedup(topo, speedup_factor1, speedup_factor2)
% experimental function to include the effects of wind speedup over steep
% topography. Implemented functions of Momiji et al (2000) and Bishop et al
% (2002). As Arens et al (1995) and Hesp (2013) mention, at the foredune
% crest wind velocity may be 50% higher. Perhaps this speed-up is not enough to
% justify an increase in slab hop length from 1 to 2, as that would mean a 100%
% increase.
% topo = read_profile_from_netcdf(3,20000,2002,0);
% topo = create_topographies(topo,0); 
% topo = topo./0.1;
% speedup_factor1 = 0.01;
% speedup_factor2 = 0.0002;
h_ref = 10;
L0 = 1;

Lup = L0 + speedup_factor1 * (topo-h_ref) + speedup_factor2 * (topo-h_ref).^2;
Lup = Lup .* (topo>h_ref);
Ldown = L0 + speedup_factor1*(topo-h_ref);
Ldown = Ldown .* (topo<=h_ref);
L = Lup+Ldown;

r1 = round(L);
% imagesc(r1)



