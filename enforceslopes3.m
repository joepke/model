function [r1,r2] = enforceslopes3(topof, vegf, slabheightf, repose_baref, repose_vegf, repose_thresholdf)

% function to enforce the angle of repose
% topof                 : topography map [topo]
% vegf                  : map of combined vegetation effectiveness [veg] [0,1]
% slabheightf           : slab height [slabheight]
% repose_baref          : angle of repose for bare sand [repose_bare]
% repose_vegf           : angle of repose for vegetated cells [repose_veg]
% repose_thresholdf     : switching threshold [repose_threshold]
% returns an updated topography 

% modifications:
% open boundaries (18 oct 2010)
% rewritten to get correct steepest slope (20 aug 2011)


% maximum allowed angle depending on vegetation cover [degrees]
maxangle    = repose_baref .*(vegf <= repose_thresholdf) + repose_vegf.*(vegf > repose_thresholdf); 

[M,N]       = size(topof);        % retrieve dimensions of area
tanslopes   = zeros(M,N,8);       % pre-allocating matrix to improve calculation speed
exceeds     = zeros(M,N,8);         

total       = 0;                  % number of avalanches
slabsmoved  = 1;                  % initial number to get the loop going
% avcount=0;                        % for debugging

while slabsmoved > 0 
    % % for debugging: turn this on to check if the model is in a loop
    %     avcount = avcount + 1;
    %     disp(avcount)

    % padding on all edges
    central = topof;    
    topof   = [topof(:,1), topof, topof(:,N)];   % padding with same cell to create open boundaries
    topof   = [topof(1,:); topof; topof(M,:)];
    
    % all height differences relative to centre cell (positive is sloping upward away, negative is sloping downward away)
    % directions are clockwise starting from upward/North [-1,0]=1, and so-on
    % corrected for slabheigt (as ratio) and diagonals to obtain tangens of
    % slope
    tanslopes(:,:,1) = (topof(1:end-2, 2:end-1) - central) .* slabheightf;    % dir 1 [-1,0]
    tanslopes(:,:,5) = (topof(3:end, 2:end-1)   - central) .* slabheightf;    % dir 5 [+1,0]
    tanslopes(:,:,3) = (topof(2:end-1, 3:end)   - central) .* slabheightf;    % dir 3 [0,+1]
    tanslopes(:,:,7) = (topof(2:end-1, 1:end-2) - central) .* slabheightf;    % dir 7 [0,-1]
    tanslopes(:,:,2) = (topof(1:end-2, 3:end)   - central) .* slabheightf ./ sqrt(2);    % dir 2 [-1,+1]
    tanslopes(:,:,4) = (topof(3:end, 3:end)     - central) .* slabheightf ./ sqrt(2);    % dir 4 [+1,+1]
    tanslopes(:,:,6) = (topof(3:end, 1:end-2)   - central) .* slabheightf ./ sqrt(2);    % dir 6 [+1,-1]
    tanslopes(:,:,8) = (topof(1:end-2, 1:end-2) - central) .* slabheightf ./ sqrt(2);    % dir 8 [-1,-1]
    
    % transform into slopes in [degrees]
    slopes = atand(tanslopes);
    
    % identify for each cell the value of the lowest (negative) slope of
    % all 8 directions [degrees]
    minima = min(slopes,[],3);   
    
    % Gives 1 if steepest slope is in this direction & is more negative than the repose limit
    for d = 1:8
    exceeds(:,:,d) = (minima == slopes(:,:,d)) & (slopes(:,:,d) < -maxangle);
    end
    
    % If there are multiple equally steepest slopes that exceed the angle
    % of repose, one of them needs to be assigned and the rest set to 0.
    k = find(sum(exceeds,3)>1); % identify cells with multiple equally steepest minima in different directions that all exceed repose angle
    if ~isempty(k)                                        % if there are such cells
        for i = k'
            [row,col] = ind2sub(size(sum(exceeds,3)),i);  % recover row and col #s from the linear index k
            a1        = rand(1,1,8).* exceeds(row,col,:); % give all equally steepest slopes in this cell a random number
            exceeds(row,col,:) =  a1 == max(a1);          % pick the largest random number and set the rest to zero
        end
    end

    % let the avalanching begin...
    topof(2:end-1,2:end-1) = ...
        topof(2:end-1,2:end-1)...
        -exceeds(:,:,1) + circshift(exceeds(:,:,1), [-1, 0])...
        -exceeds(:,:,5) + circshift(exceeds(:,:,5), [ 1, 0])...
        -exceeds(:,:,3) + circshift(exceeds(:,:,3), [ 0, 1])...
        -exceeds(:,:,7) + circshift(exceeds(:,:,7), [ 0,-1])...
        -exceeds(:,:,2) + circshift(exceeds(:,:,2), [-1, 1])...
        -exceeds(:,:,4) + circshift(exceeds(:,:,4), [ 1, 1])...
        -exceeds(:,:,6) + circshift(exceeds(:,:,6), [ 1,-1])...
        -exceeds(:,:,8) + circshift(exceeds(:,:,8), [-1,-1]);
                        
    topof         = topof(2:end-1,2:end-1);  % remove padding
    slabsmoved    = sum(exceeds(:));         % count moved slabs during this loop
    total         = total + slabsmoved;      % total number of moved slabs since beginning of m-file
end

r1 = topof;
r2 = total;