%% run dune simulation model (model.m) with parameters specified below
% profile on
clear all; close all;
isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0;
if isOctave
	pkg load netcdf statistics
end

% set seed for random number generator 
rand('state', 1234)
% parameters
area_code = 3;
start_year = 2002;
simulation_time_y = 9;
profile_id = 20000;

    
% read JARKUS profile from NetCDF file
[topoin, vegin] = read_profile_from_netcdf(area_code, profile_id, start_year, 0);

% get default model input data:
OPT = model_input;

%% Model run configuration
OPT.zInitial =  topoin';
OPT.zInitial =  vegin';
OPT.area_code =  area_code;
OPT.profile_id = profile_id;
OPT.start_year = start_year;
OPT.simulation_time_y = simulation_time_y;

%% Modify settings if necessary: 
i = 1;
for pv = (2:5)/10
    for dl = (1:6)/10
        for di = 0.010:0.0005:0.015
            for r = 1
                rand('state', r)

                set_value(OPT, 'repose_bare', 30);
                set_value(OPT, 'repose_vegetated', 35);
                set_value(OPT, 'p_deposition_bare', 0.13);
                set_value(OPT, 'p_deposition_veg', pv);
                set_value(OPT, 'iterations_per_cycle', 52);
                set_value(OPT, 'sp1_b', 0.05);
                set_value(OPT, 'depth_limit', dl);
                set_value(OPT, 'dissipation_rate', di);

                % run simulations and get output results
                disp(i)
                [OUT] = model_sens(OPT);
                
                fn = ['./temp/calibration_pv_', num2str(pv, '%02.1f'), '_dl_', num2str(dl, '%02.1f'), '_di_', num2str(di, '%03.4f'), '_r', num2str(r),'.mat']
%                 fn = './temp/test.mat'
                save(fn, 'OUT');
                i = i + 1;
            end
        end
    end
    
    
end
    
% profile off
% profile viewer