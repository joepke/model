%% profile development
j =  read_profile_from_netcdf(20000,2000,10);
figure; hold on;
set(gca,'ColorOrder',jet(size(j,2)))
plot(j)%,'Displayname',sprintfc('y %i',1:size(topos,2))); title('modeled')
ylim([0,15])
%legend('location','northwest')
xlabel('cross-shore distance (m)')
ylabel('elevation (m)')

