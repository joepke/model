function [topo, eqtopo, spec1at0, spec2at0] = create_topographies(topoin, vegin)
% create matrix of initial topography, equilibrium topography and initial
% vegetation, based on JARKUS profile extract (topoin)

%% Uncomment line below to run standalone
% [topoin, vegin] = read_profile_from_netcdf(3, 20000, 2002, 0);

%% parameter settings
footlevel = 2.5; % in m + NAP
longshore = 25; % extent in m
crossshore = length(topoin);  

%% find foredune crest, necessary for setting right vegetation cover
x = topoin(:,1);
% Unwrap to vector
x = x(:);
% Identify whether signal is rising or falling
upordown = sign(diff(x));
% Find points where signal is rising before, falling after
maxflags = [upordown(1)<0; diff(upordown)<0; upordown(end)>0];
maxima   = find(maxflags);
crest = find(x(maxima)>6,1,'first');
crest = find(x==x(maxima(crest)),1,'first'); 

%% select beach part and fit equilibrium topography
foot = find(topoin>=footlevel,1,'first');
zz = topoin(1:foot);
xx = 1:crossshore;
a = (zz(foot)-zz(1))/(xx(foot)-xx(1));
eqt = xx*a;
% eqt = transp(topoin(1:foot));

%% create 2D topographies by stacking to matrix
topo   = repmat(transpose(topoin),longshore,1);
eqtopo = repmat(eqt,longshore,1);

% check if vegetation is already known from NetCDF profile in
% read_profile_from_ncdf.m ...
if ~all(isnan(vegin))
    
    % vegetation data ARE available
    % replace NaNs on the beach with 0
    vegin(isnan(vegin)) = 0;
    
    % replace data on landward side of crest with 1
    vegin(crest:end) = 1;
    
    % stack array to matrix
	spec1at0 = repmat(transpose(vegin),longshore,1);
    
else
    
    % vegetation data ARE NOT available
	% create initial vegetation map, uses a logistic function

	MHT              = 2; % not really related to MHT. higher values cause more landward start of vegetation and less vegetation cover in lower parts
	m3_1             = 1;      
	m4_1             = 1;     
	m5a_1            = 3;     
	m5_1             = -(MHT + m5a_1);    
	% vegetation as function of initial elevation
	spec1at0         = 1./(1 + m3_1.*exp(-m4_1.*(topo + m5_1)));   
    
end

% add scatter to mimic natural variability
% scatamp          = 0.2;
% scatter          = scatamp .* (rand(size(spec1at0)) - 0.5); 
% spec1at0         = spec1at0 - scatter;
% spec2at0         = spec2at0 - scatter;

% remove pioneers from landward side
spec1at0(:,(crest+20):end) = 0;

% second species only landwards of crest
spec2at0 = zeros(size(spec1at0));
spec2at0(:,crest:end) = 1-spec1at0(:,crest:end);  

% limit to [0 1]
spec1at0(spec1at0<0) = 0;
spec1at0(spec1at0>1) = 1;
spec2at0(spec2at0<0) = 0;
spec2at0(spec2at0>1) = 1;

% remove vegetation from beach
spec1at0(:,1:foot) = 0;  
spec2at0(:,1:foot) = 0;  


%% plots for testing
% figure;
% subplot(4,1,1)
% plot(topo(1,:))
% subplot(4,1,2)
% plot(spec1at0(1,:))
% subplot(4,1,3)
% plot(spec2at0(1,:))
% subplot(4,1,4)
% plot(spec2at0(1,:)+spec1at0(1,:))