function [r1, r2, r3, r4] = test_beachupdate(ar, pr, yr, waterlevel, slabheight, cellsize, ...
     m26f, m27af, m28f, pwavemaxf, pwaveminf , depthlimitf, shelterf)


[topoin, vegin] = read_profile_from_netcdf(ar, pr, yr, 0);
[topo, eqtopo, veg1, veg2] = create_topographies(topoin, vegin);
veg = veg1+veg2;
begintopo=topo;

waterlevel_sl = (waterlevel)/slabheight;
eqtopo    = eqtopo./slabheight;     % transform from m into number of slabs
topo      = topo./slabheight; %slabs]
gw        = topo .* 0.9;  

% determine dune volume before ... 
topo3 = topo >= 30;
topovol = topo3.*topo;
V1 = sum(topovol(:))/size(topo,1)*slabheight;
F1 = find(mean(topo,1)>waterlevel_sl,1,'first');

[topo, ~, ~, ~, ~, ~] = marine_processes(waterlevel_sl, 0, slabheight, cellsize, topo, eqtopo ,veg,  ...
     m26f, m27af, m28f, pwavemaxf, pwaveminf , depthlimitf, shelterf);
 
% ... and after beachupdate
topo3 = topo >= 30;
topovol = topo3.*topo;
aftertopo = topo*slabheight;
V2 = sum(topovol(:))/size(topo,1)*slabheight;
F2 = find(mean(topo,1)>waterlevel_sl,1,'first');
    

r1 = topoin;
r2 = mean(aftertopo,1);
r3 = V2 - V1;
r4 = F2 - F1;

% 
%     %% plot results
%     % current column number
%     cc = a;     
% 
%     nc = length(items);% number of columns
%     nr = 6;% number of rows (different plots)
%     
%     

% figure;
% x = 1:numel(topoin);
% y1 = transpose(topoin);
% y2 = mean(aftertopo,1);
% plot(topoin,'displayname','after transport'); hold on; % plot([1,size(topo,2)],[waterlevel, waterlevel],['--r']);
% plot(mean(aftertopo,1),'m','displayname','after erosion'); 
% xpol =[x,fliplr(x)];                %create continuous x value array for plotting
% ypol =[y1,fliplr(y2)];              %create y values for out and then back
% fill(xpol,ypol,'r');                  %plot filled area
% plot([min(x), max(x)], [waterlevel, waterlevel])
% plot([min(x), max(x)], [(waterlevel+X), (waterlevel+X)])
%     %%
% 
%     
% end
% 
% 
% %% plot storm surge level versus eroded volume
% figure;
% % model
% plot(waterlevels,dVs,'r*-');xlabel('storm surge level (m)');ylabel('\Delta V (m^3/m/y)');
% hold on
% 
% % add measured
% dV_prof = dV.dV(find(dV.along==pr, 1),:);
% plot(maxsealevels.max,dV_prof,'bo')
% plot([min(waterlevels) max(waterlevels)], [0 0], 'k--')
% % plot(maxsealevels.max,dV.dV(:,:),'bo')
% ylim([min([dVs,dV_prof]),40])
% % fn=['erosion_pr',num2str(pr),'.tif'];
% % save_fig(gcf,fn,4,3,300)
% 
% %saveas (1, "figure1.png");
% if ~exist('D:','dir')
%     pause
% end
% 
