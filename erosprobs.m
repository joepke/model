function r=erosprobs(vegf, shade, sand, topof, groundw, perosion)
% to return a map with erosion probabilities
% vegf      : map of combined vegetation effectiveness [veg] [0,1]
% shade     : logical map of shadowzones [shadowmap]
% sand      : logical map of sandy cells [sandmap]
% topof     : topography map [topo]
% groundw   : groundwater map [gw]
% perosion  : probability slab is picked up [0 1]
% returns a map of erosion probabilities

r=(~shade).*sand.*(perosion-vegf).*(topof>groundw);         % pretty simple, huh?
