function [r1, r2, r3, r4, r5] = get_dune_statistics(x, z, foot_level, crest_level)
%GET_PROFILE_VOLUME calculates the sand volume stored in a dune above a
%reference level. 
%% uncomment to test
% reference_level = 3;
% x = linspace(1,1000,length(topo));
% z = read_profile_from_netcdf(3,20000,2011,0);
% plot(x, z); hold on
% plot(x, z .* (z>=reference_level), ' r--' )

%% determine volume above reference level with trapezoidal rule
if max(z) < foot_level
    warning('DUBEVEG:dunefoot_level_error','No cells above dune-foot level.')
    [V, F, Zmax, Zpos] = deal(nan);
else
    [Zmax, Zpos] = max(z);
    F = find(z>=(foot_level), 1, 'first');
    z_temp = z - foot_level;
    V = trapz(x, z_temp .* (z_temp>=0));
end

if Zmax > crest_level
    upordown = sign(diff(x));
    maxflags = [upordown(1)<0; diff(upordown)<0; upordown(end)>0];
    maxima   = find(maxflags);
    crest    = find(x(maxima)>crest_level,1,'first');
    crest    = find(x==x(maxima(crest)),1,'first'); 
end

r1 = V;
r2 = F;
r3 = Zmax;
r4 = Zpos;
r5 = first_crest_position;

