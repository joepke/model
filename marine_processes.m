function [r1,r2,r3,r4,r5,r6] = marine_processes(total_tide, msl, slabheight, cellsizef,...
                                topof, eqtopof, vegf, m26f, ...
                                m27af, m28f, pwavemaxf, pwaveminf, ...
                                depthlimitf, shelterf)
%BEACHUPDATE calculates the effects of high tide levels on the beach and foredune       
% Beachupdate in cellular automata fashion
% Sets back a certain length of the profile to the equilibrium;
% if waterlevel exceeds dunefoot, this lead to dune erosion;
% otherwise, only the beach is reset.
% 
% tide          : height of storm surge [slabs]
% slabheight    : slabheight in m [slabheightm] [m]
% cellsizef     : interpreted cell size [cellsize] [m]
% topof         : topography map [topo] [slabs]
% eqtopof       : equilibrium beach topography map [eqtopo] [slabs]
% vegf          : map of combined vegetation effectiveness [veg] [0-1]
% m26f          : parameter for dissipation strength ~[0.01 - 0.02]
% m27af         : wave energy factor
% m28f          : resistance of vegetation: 1 = full, 0 = none. 
% pwavemaxf     : maximum erosive strenght of waves (if >1: overrules vegetation)
% pwaveminf     : in area with waves always potential for action
% depthlimitf   : no erosive strength in very shallow water
% shelterf      : exposure of sheltered cells: 1 = full shelter, 0 = no shelter.

%% activate this to test standalone
% [topo, veg] = read_profile_from_netcdf(3, 20000, 1997,0);
% [topo, eqtopo, veg1, veg2] = create_topographies(topo, veg);
% slabheight   = 0.1;
% depthlimitf  = 0.4;     % strongly controls the retreat distance 
% cellsizef    = 1;
% topof        = topo./slabheight;
% old_topof    = topof;
% eqtopof      = eqtopo./slabheight;
% vegf         = veg1+veg2;
% test         = 1;
% m26f         = 0.013;   % dissipation strength
% m27af        = 1;       % wave energy factor (redundant?)
% m28f         = 0.0;     % resistance of vegetation (1 = full)
% pwavemaxf    = 1;
% pwaveminf    = 1;       % if 1: always erosion if inundated; if 0: no erosion if all energy has been dissipated
% shelterf     = 1.0;     % exposure to waves in sheltered cells (1 = full shelter, 0 = no shelter)
% total_tide   = 45; % offshore tide level [slabs]
% msl          = 0;

%% Wave runup
% offshore measured tide (sealevf) has to be converted to effective tide
% level at the shoreline. The vertical limit of wave runup (R) is a function of
% tide, slope and wave conditions. Since wave conditions are correlated
% with tide level (higher waves associated with higher storms, we use a
% simplified expression where R = f(tide, slope).

% Original expression of wave runup height controlled by foreshore slope
% (Stockdon et al, 2006):
% Irribarren number = b/sqrt(H/L)
% for dissipative beaches (Irribarren number < 0.3):
%   runup_m = 0.043 * sqrt(H*L);
% for intermediate or reflective beaches (Irribarren number >= 0.3):
%   runup = 1.1*(.35*tan(b)*sqrt(H*L) + sqrt(H*L*(0.563*tan(b^2)+0.004))/2);

% derive gradient of eqtopo as an approximation of foreshore slope
b = mean(gradient(eqtopof(1,:)*slabheight));

% tidal elevation above MSL:
tide = total_tide - msl;
tide_m = tide * slabheight;
msl_m = msl * slabheight;

H =  max(0, -2.637 + 2.931*tide_m);
L =  max(0, -30.59 + 46.74*tide_m);  
% disp(['Irribarren: ', num2str(b/sqrt(H/L)),'; H: ', num2str(H), '; L:', num2str(L)])

% runup as a function of wave conditions (H, L) and foreshore slope (b)
if b/sqrt(H/L) < 0.3
    runup_m = 0.043 * sqrt(H*L);
else
    runup_m = 1.1*(.35*tan(b)*sqrt(H*L) + sqrt(H*L*(0.563*tan(b^2)+0.004))/2);
end
% add runup to tide to arrive at total water level (=tide + runup + msl)
totalwater_m = tide_m + runup_m + msl_m;
totalwater = totalwater_m / slabheight;

% X = ['tide :', num2str(tide_m), 'runup_m :', num2str(runup_m), 'msl_m', num2str(msl_m)];
% disp(X)

%% Identify the cells that are exposed to waves
% by dunes and embryodunes, 
% analogous to shadowzones but no angle
% toolow  = topof < sealevf;  % give matrix with cells that are potentially under water [binary]
pexposed = ones(size(topof)) ;                          % initialise matrix
for m20 = 1: length(topof(:,1))                         % run along all the rows
    m21 = find(topof(m20,:) >= totalwater, 1, 'first');    % finds for every row the first instance where the topography exceeds the sea level
    pexposed(m20,m21:end) = 1 - shelterf;               % subtract shelter from exposedness: sheltered cells are less exposed
end

%% Fill topography to equilibrium

inundatedf  = pexposed;             % inundated is the area that really receives sea water
% [M,N]       = size(inundatedf);     % retrieve dimensions of area
% inundatedf  = [zeros(M,1), inundatedf, zeros(M,1)];   % pad the matrices with zeros
% inundatedf  = [zeros(1,N+2); inundatedf; zeros(1,N+2)];
% toolow      = [zeros(M,1), toolow, zeros(M,1)];
% toolow      = [zeros(1,N+2); toolow; zeros(1,N+2)];
% addedcells  = ones(size(toolow));  

% while sum(addedcells(:)) > 0        % run for as long as there are cells added
%     inundatedfbefore  = inundatedf;     % make a copy
%     inundatedf        = inundatedf | (toolow .* (circshift(inundatedf, [0, 1]) | ...
%                         circshift(inundatedf, [0, -1]) | ...
%                         circshift(inundatedf, [1, 0]) | ...
%                         circshift(inundatedf, [-1, 0]) ));
%     addedcells        = inundatedf - inundatedfbefore;
% end
% inundatedf  = inundatedf(2:end-1,2:end-1);      % remove padding


%% Waves
waterdepth  = (totalwater - topof).*pexposed.*slabheight;    % in [m], exposed is used to avoid negative waterdepths
waterdepth(waterdepth<= depthlimitf) = depthlimitf;			% this limits high dissipitation when depths are limited; remainder of energy is transferred landward

% initialise dissiptation matrices
diss        = zeros(size(topof));       
cumdiss     = zeros(size(topof));

% calculate dissipation
diss(:,1)   = cellsizef./waterdepth(:,1);                   % dissipation corrected for cellsize
cumdiss(:,1)= diss(:,1);
for m25 = 2 : size(topof,2)                                 % do for all columns
    diss(:,m25)    = cellsizef ./ waterdepth(:,m25);        % dissipation corrected for cellsize
    cumdiss(:,m25) = diss(:,m25-1) + cumdiss(:,m25-1);      % cumulative dissipation from the shore, excluding the current cell
end

% initial wave strength m27f
m27f = m27af*totalwater;

% dissipation of wave strength across the topography (wave strength times dissiptation)
pwave                    = (pwavemaxf - m26f.*cumdiss).*m27f; 
pwave( (pwave < pwaveminf) & (topof < totalwater) & (pexposed > 0)) = pwaveminf;                        % in area with waves always potential for action
% pwave(waterdepth < slabheightmf.*depthlimitf) = 0;                  % no erosive strength in very shallow water

% local reduction of erosive strength due to vegetation
pbare       = 1 - m28f.*vegf;    % if vegf = 1, still some chance for being eroded

% updating the topography
pbeachupdate = pbare .* pwave; %.* pexposed; % probability for beachupdate, also indication of strength of process (it does not do anything random)
pbeachupdate(pbeachupdate<0) = 0;                                  % keep probabilities to 0 - 1 range
pbeachupdate(pbeachupdate>1) = 1;

% changed after revision (JK 21/01/2015)
% limit filling up of topography to the maximum water level
% so added (accreted) cells cannot be above the maximum water level
% eqtopof( (eqtopof>sealevf)) = sealevf;
topof        = topof - (topof - eqtopof) .* pbeachupdate;            % adjusting the topography
topof( (topof > totalwater) & (pbeachupdate>0) ) = totalwater;

if exist('test','var')
    figure;
    m = 3; n=2;
    subplot(m,n,1); plot(mean(topo,1)); hold on; plot(mean(topof,1).*slabheight,'r'); ylabel('topo')
    subplot(m,n,2); plot(mean(waterdepth,1)); ylabel('waterdepth')
    subplot(m,n,3); plot(mean(inundatedf,1)); ylabel('inundated')
    subplot(m,n,4); plot(mean(cumdiss,1)); ylabel('cumdiss')
    subplot(m,n,5); plot(mean(pwave,1)); ylabel('pwave')
    subplot(m,n,6); plot(mean(pbeachupdate,1)); ylabel('pbeachupdate')
    
    figure;
    x = 1:numel(mean(topo,1));
    y1 = mean(topo,1);
    y2 = mean(topof,1).*slabheight;
    plot(mean(topo,1),'displayname','after transport'); hold on; % plot([1,size(topo,2)],[waterlevel, waterlevel],['--r']);
%     plot(mean(topof,1).*slabheightmf,'m','displayname','after erosion'); 

    xpol =[x,fliplr(x)];                %create continuous x value array for plotting
    ypol =[y1,fliplr(y2)];              %create y values for out and then back
    fill(xpol,ypol,'r');                  %plot filled area
    plot(mean(eqtopof,1).*slabheight, 'g-')
%     plot([min(x), max(x)], [sealevel, sealevel])
    plot(xlim, [(totalwater_m), (totalwater_m)])
    plot(xlim, [tide_m tide_m],'b--')
%     plot(xlim, b*find(pbeachupdate>0,1,'last'))
%     plot([min(x), max(x)], [(sealevel+X), (sealevel+X)])

end
% results
r1 = topof;
r2 = inundatedf;
r3 = pbeachupdate;
r4 = diss;
r5 = cumdiss;
r6 = pwave;
