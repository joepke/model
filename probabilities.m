%% probabilities
% veg = (0:10)/10;
% 
% dep1 = 0.6;
% pdep = veg*(1-dep1) + dep1;  
% pero  = 1-veg;
% 
% figure; hold on
% plot(veg,pdep,'b-o','displayname','erosion')
% plot(veg,pero,'r-*','displayname','deposition')
% xlabel('vegetation effectiveness (-)')
% ylabel('P')
% legend({'deposition','erosion'},'location','west')

%% sandbox
close all
clear all

% parameters
p_dep_sand = 0.6;
p_dep_bare = 0.4;
gw_reduction = 0.6;
jumplength=1;
wakelength=0;
vegmax=1;
slabheight=0.1;

% initial maps
west  = 3000:200:5000;
middle = 9000:200:11000;
east = 19000:200:21000;
prs = [middle, east];


pr = 21000;
[topoins, vegin] = read_profile_from_netcdf(pr, 2002, 3, 0);
measured_diff = (topoins(:,end) - topoins(:,1)) .* (topoins(:,1)>3);
measured_gain = sum(measured_diff(~isnan(measured_diff)));
topoin = topoins(:,1);
[topo, eqtopo, veg0, veg1] = create_topographies(topoin,vegin);
%veg0 = circshift(veg0,[0 20]);
%veg0(:,1:20) = 0;
topo0         = round(topo ./ slabheight); 
shadowmap = zeros(size(topo0));
sandmap = zeros(size(topo0))+1;
gw = gw_reduction.*topo0;

as = [0:2]*2; % wakes
bs = [1:3]; % jump lengths
corrs = nan(numel(as),numel(bs));


for a = 1:length(as)
    wakelen = as(a);
    
    for b = 1:length(bs)

        topo = topo0;
        veg = add_wakes(veg0+veg1,wakelen);
        veg(veg>1) = 1;

        jumplength=bs(b);
                     
        totaltransp=0;
        i=0;
        gain = 0;
        while gain < measured_gain
            erosmap       = erosprobs(veg, shadowmap, sandmap, topo, gw); 
            deposmap      = depprobs(veg, shadowmap, sandmap, p_dep_bare, p_dep_sand); 
            [changemap,slabtransp,pickedup] = shiftslabs3(erosmap, deposmap, jumplength);  
            topo          = topo+changemap;   
            ytopo0 = mean(topo0,1)*slabheight;
            ytopo1 = mean(topo,1)*slabheight;
            difftopo = ytopo1-ytopo0;
            difftopo = difftopo(~isnan(difftopo));
            [topo, aval]  = enforceslopes3(topo, veg, 0.1, 30, 40, 0.3); % enforce angles of repose: avalanching
            gain = sum(difftopo.*(difftopo>0));
            i=i+1;
        end
        
%         X = sprintf('wake = %i, jump = %i, iter = %i, transp = %i', wakelen, jumplength, i, totaltransp);
%         disp(X)
        
        %% start plot

        h=figure('visible','off');
        x = 1:size(topo0,2);
        ytopo0 = mean(topo0,1)*slabheight;
        ytopo1 = mean(topo,1)*slabheight;
        
        % plot measured topography
        subplot(3,1,1); hold on;
        set(gca,'ColorOrder',bone(size(topoins,2)))
        ylim([0 20])
        plot(topoins,'color',[0.4,0.4,0.4])
        plot(topoins(:,1),'r')
        plot(topoins(:,end),'b')
        
        % plot modeled topography
        subplot(3,1,2);
        % fill area gained
        X=[x(~isnan(ytopo0)),fliplr(x(~isnan(ytopo0)))];                
        Y=[ytopo0(~isnan(ytopo0)),fliplr(ytopo1(~isnan(ytopo0)))]; 
        f = fill(X,Y,'r');   hold on
        set(f,'EdgeColor','None');
        plot(ytopo0,'k','displayname','initial')      
        plot(ytopo1,'b','displayname','after transport')
        plot(mean(veg0,1),'g-')

        title('topography','Fontsize',10)
        hold off
        
        %% plot changes in topography 
        subplot(3,1,3); 
        x = 1:5:size(topoins,1);
        modeled = ytopo1-ytopo0;
        modeled = modeled(x);
        measured = (topoins(:,end) - topoins(:,1));
        measured = transp(measured(x));
       
        title('sedimentation','Fontsize',10)
        
        plot(x, modeled,'k*-','displayname','sedimentation')
        hold on
%         orig_topo = topoins(1:5:end,:);
        y = measured_diff(x);%/size(topoins,2);
        plot(x, measured,'r-o','markerfacecolor','r','markersize',2)       
        ylim([-1 4])
        plot([0 max(x)], [0 0],'k--')
        hold off
       
        % Nash-Sutcliffe
        success = 1 -  (sum( (measured - modeled).^2)) / ( sum( (measured - mean(measured)).^2));
        text(0,0.5,num2str(success));
        
        %% save plot
        fn=['sandbox_pr',num2str(pr),'_wake',num2str(wakelen),'_jump',num2str(jumplength)];
        save_fig(h, fn, 6, 4, 300) 

        
    end % jump
end % wake
