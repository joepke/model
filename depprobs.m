function r=depprobs(vegf, shade, sand, dep0, dep1)
% to return a map of deposition probabilities that can then be used
% to implement transport
% vegf          : map of combined vegetation effectiveness [veg] [0,1]
% shade         : logical map of shadowzones [shadowmap]
% sand          : logical map of sanded sites [sandmap]
% dep0          : deposition probability on bare cell [p_dep_bare]
% dep1          : deposition probability on sandy cell [p_dep_sand]
% returns a map of deposition probabilities

% for bare cells:
temp1=vegf*(1-dep0) + dep0;     % deposition probabilities on bare cells
temp2=~sand.*~shade.*temp1;     % apply to bare cells outside shadows only

% for sandy cells:
% temp3=vegf*(1-dep1) + dep1;     % deposition probabilities on sandy cells
% temp3=vegf*0.09117903 + dep0;   %(from Geomorphology paper)
temp3=(dep1-dep0)*vegf + dep0;   %(from Geomorphology paper)
temp4=sand.*~shade.*temp3;      % apply to sandy cells outside shadows only

r=temp2+temp4+shade;            % combine both types of cells + shadowzones