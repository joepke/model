%% run dune model, supplied with a climate scenarios and parameters specified below


%% settings
clear all; close all;

% parameters
% number of runs:
number_of_runs = 1;
simulation_time_y = 99;
start_year = 2002;

% profiles and areacodes for runs
ars = [3     3     4     4    ];
prs = [17000 20000 22000 13000];
sls = [0.0   0.0   0.0   0.0  ];

perturbation_time = -1;

%% run for four profiles
for i = 2
    
    area_code = ars(i);
    profile_id = prs(i);
    sl = sls(i);

    for SLR = 0.5
              
                
        for VGR = -100:50:100
                        
            % matrix for storing output variables for each run
            [Fs, Vs, Cs] = deal(nan(number_of_runs, simulation_time_y+1));
            
            for r = 1:number_of_runs
                rand('state', r)
                run_number = r;
                close all
                X = ['profile: ', num2str(profile_id),'; SLR: ', num2str(SLR),'; VGR: ',num2str(VGR),'; run #: ',num2str(r)];
                disp(X)
                
                OPT = model_input;
                OPT.perturbation_time = perturbation_time;
                set_value(OPT, 'profile_id', profile_id);
                set_value(OPT, 'area_code', area_code);
                set_value(OPT, 'simulation_time_y', simulation_time_y);
%                 set_value(OPT, 'dissipation_rate', 0.015);
%                 set_value(OPT, 'depth_limit', 0.4);
%                 OPT = set_value(OPT, 'runup_a', 100);
             
                OUT = model_scenario(OPT, SLR, VGR, run_number);
                OUT.run = run_number;
%                 Fs(r,:) = OUT.F;
%                 Vs(r,:) = OUT.V;
%                 Cs(r,:) = OUT.C;
                
                
%                 csvwrite(['./temp/SLR_',num2str(SLR),'_VGR_',num2str(VGR),'_',num2str(run_number),'at_t_25.csv'], C)
                fn = ['C:/joep/Dropbox/model/perturbation/no_perturbation_100m_landward_10m_longshore_',num2str(simulation_time_y),'y_', num2str(profile_id),'_',num2str(area_code),'_SLR_',num2str(SLR, '%+03.1f'),'_VGR_',num2str(VGR, '%+04.0f'),'_', num2str(run_number),'_Ptime_',num2str(perturbation_time, '%+03.1f'),'.mat'];
                save(fn, 'OUT')

            end % run number
        end % VGR
    end % SLR


    %             fn = ['D:/Dropbox/model/scenarios/F_climate-change_vegred75_',num2str(ar),'_',num2str(pr),'_',num2str(r),'_sl_',num2str(sl),'.csv'];
%     fn = ['D:/Dropbox/model/scenarios/F_climate-change_NEW_',num2str(ar),'_',num2str(pr),'_SLR_',num2str(SLR),'VGR',num2str(VGR),'.csv'];
%     csvwrite(fn,Fs)
    %             fn = ['D:/Dropbox/model/scenarios/V_climate-change_vegred75_',num2str(ar),'_',num2str(pr),'_',num2str(r),'_sl_',num2str(sl),'.csv'];
%     fn = ['D:/Dropbox/model/scenarios/V_climate-change_NEW_',num2str(ar),'_',num2str(pr),'_SLR_',num2str(SLR),'VGR',num2str(VGR),'.csv'];
%     csvwrite(fn,Vs)
    %             fn = ['D:/Dropbox/model/scenarios/topos_at_2050_vegred75_',num2str(ar),'_',num2str(pr),'_',num2str(r),'.csv'];
%     fn = ['D:/Dropbox/model/scenarios/tpos_at_2050_climate-change_NEW_',num2str(ar),'_',num2str(pr),'_SLR_',num2str(SLR),'VGR',num2str(VGR),'.csv'];
% %     csvwrite(fn,topos)

end % profile


