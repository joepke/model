function [zi, vi] = read_profile_from_netcdf(areacode, my_tr, my_yr, timewindow)
% areacode = 3 (Ameland), areacode 4 (Terschelling)
% read JARKUS profile from NetCDF file NetCDF file - obtained from
% OpenEarth (http://opendap.deltares.nl/thredds/catalog/opendap/rijkswaterstaat/jarkus/profiles/catalog.html?dataset=varopendap/rijkswaterstaat/jarkus/profiles/transect.nc)
% This file ontains all yearly JARKUS profile measurements from 1965 - now.
% Elevation data are supplied as a 3D matrix: [cross shore, alongshore,
% time]. Elevation data for a given transect 'tr' extracted, flipped and
% cropped to the beach and dune (>0 m NAP) section and interpolated to 1 m
% cross-shore resolution. The returned matrix contains two columns, one
% with elevation data of year = yr and one with data of year =
% yr+timewindow.

%% NetCDF description
% time: days since 1970-1-1
% id: sum of area code (x1000000) and alongshore coordinate
% altitude: altitude above geoid (NAP) Dimensions: cross_shore,alongshore,time;       
            
%% uncomment parameters below to run standalone:
% areacode = 3;
% my_tr = 16600;
% my_yr = 2002;
% timewindow=9;

%% set parameters and directory
landward = 50;          % (m) landward extent as landward distance from crest, multiple of 5
hor_res  = 5;           % (m) horizontal resolution of JARKUS
min_crest_height = 6;   % (m) minimum height of a local maximum before it is called a crest

% set data source for Ameland (3) or Terschelling (4)
if areacode == 3
	ncprof = './data/model_ameland.nc';
%     ncprof = './data/ameland_lufo2.nc';
end
if areacode == 4
	ncprof = './data/model_terschelling.nc';
end
	
%% read NetCDF dimensions
yr  = ncread(ncprof,'time');
tr  = ncread(ncprof,'alongshore'); % 'id: sum of area code (x1000000) and alongshore coordinate'

% index positions of transect
tr_pos = find(tr==my_tr);
yr_pos = find(yr==my_yr);
if isempty(tr_pos) || isempty(yr_pos)
    error('PROFILE:NOT_FOUND','No JARKUS data found for:\n transect: %g; area: %g; year: %g.', my_tr, areacode, my_yr)
end

z = ncread(ncprof, 'z',[1,tr_pos,yr_pos],[Inf,1,timewindow+1]);
z = squeeze(z);
z(z==-9999) = nan;

% find shoreline (z = 0 m NAP)
% select 50 most landward measurements
z = flipdim(z,1);
% shoreline = find(z(:,1)>0,1,'first');
shoreline = find(z(:,1)<0,1,'last');
z = z(shoreline:end,:);

%FINDMAXIMA  Find location of dune ridge
%  From David Sampson
% http://www-h.eng.cam.ac.uk/help/tpl/programs/Matlab/minmax.html
x = z(:,1);
% Unwrap to vector
x = x(:);
% Identify whether signal is rising or falling
upordown = sign(diff(x));
% Find points where signal is rising before, falling after
maxflags = [upordown(1)<0; diff(upordown)<0; upordown(end)>0];
maxima   = find(maxflags);
crest = find(x(maxima)>min_crest_height,1,'first');
landward_limit = find(x==x(maxima(crest)),1,'first') + landward/hor_res;

z = z(1:landward_limit,:);
x = (0:size(z,1)-1)*hor_res;

% interpolate from 5 m to 1 m cross shore resolution
xi = 1:1:max(x);

zi = interp1(x,z,xi);
if my_yr == 1997
    zi = spline(x,z,xi);
end

if timewindow==0
    zi = transpose(zi);
end;

%% extract vegetation cover from same NetCDF source

% if simulation starts in 2002 or 2004, supply vegetation cover from 2003
if (my_yr == 2002 || my_yr == 2004) && timewindow == 0
    yr_pos = find(yr==2003,1,'first');
end

% extract values
veg   = ncread(ncprof,'v',[1,tr_pos,yr_pos],[Inf,1,timewindow+1]);

% drop unused dimensions
veg = squeeze(veg);

% replace NA flag with nan
veg(veg==-9999) = nan;

% if all measurements are 0, set nan
% (still necessary?)
for nc = 1:size(veg,2)
    if all(veg(:,nc)==0)
        veg(:,nc) = nan;
    end
end

% reverse direction and limit to beach and dune zone
veg = flipdim(veg,1);
veg = veg(shoreline:end,:);
veg = veg(1:landward_limit,:);

% interpolate from 5 m to 1 m resolution
vi  = interp1(x,veg,xi);

% transpose if single array
if timewindow==0
    vi = transpose(vi);
end

%% plots for testing
% plot(zi)
% hold on
% plot(vi.*10, 'r--')