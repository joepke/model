% calculate wakes of vegetation, 'veg' is model output of vegetation
% efficiency
% dimensions
longshore = 50;
crossshore = 250;

% vegetation presence
veg1 = ~(veg==0);
veg2 = veg;

% shift vegetation i slabs landward to simulate a wake
for i=1:5
    dummy = zeros(longshore,i);
    vegshift = [dummy veg(:,1:crossshore-i)];
    veg2 = veg2+vegshift;
end

% limit to geomorphological range
veg2(veg2>1) = 1;

% indicate zonation
figure;
wakes = (veg1==0 & ~(veg2==0))*2;
vegwakes = wakes+veg1;
colormap(hsv(3))
labels={'bare', 'vegetation', 'wake'};
imagesc(vegwakes);
xlabel('cross shore (m)'); ylabel('longshore (m)')
axis equal tight;
lcolorbar(labels)

% sum of effectiveness
figure;
subplot(2,1,1);imagesc(veg);axis equal tight;
xlabel('cross shore (m)'); ylabel('longshore (m)'); title('original effectiveness')
subplot(2,1,2);imagesc(veg2);axis equal tight;
xlabel('cross shore (m)'); ylabel('longshore (m)'); title('effectiveness including wakes')