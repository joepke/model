function [r1, r2] = test_DUROS(ar, tr, yr, waterlevel)


%% activate to run standalone;
% ar = 3;
% tr = 20000;
% yr = 2000;
% waterlevel = 3; 
% test_plot = 1;

%% Initiate input variables
Hs = 6;                 % Significant wave height during the storm
Tp = 16;                % Peak wave period during the storm
WL = waterlevel;        % Maximum storm search level
Tp = 1.113 + 2.525*WL;
Hs = -2.637 + 2.931*WL;
D50 = 225e-6;           % Grain size.

%% Directory of NetCDF file with JARKUS profile data
if ~exist('D:','dir')==0
    ncfile  = 'D:/transects_all_2013.nc';
else
    ncfile  = 'C:/joep/Dropbox/other/transects_ameland_2013.nc';
end

time  = ncread(ncfile,'time');
my_time = year(time)+1970; % not working in Octave, output should be 1965:2012

%% convert input transect id to id-format in NetCDF file
% 'id: sum of area code (x1000000) and alongshore coordinate'
areacode = ar; % Ameland
my_id = areacode*1000000 + tr/10;
id    = ncread(ncfile,'id');

% index positions of tra
id_pos = find(id==my_id);
time_pos = find(my_time==yr);

%% read xz data from file
% altitude: Size:       1925x1442x48; Dimensions:
% cross_shore,alongshore,time; 
x = ncread(ncfile, 'cross_shore', 1,Inf);
z = ncread(ncfile, 'altitude',[1,id_pos,time_pos],[Inf,1,1]);
z = squeeze(z);
z(z==-9999) = nan;

%% limit to non-NA values
x=x(~isnan(z));
z=z(~isnan(z));

% Please use the following settings to run a D++ computation:
DuneErosionSettings('set','ParabolicProfileFcn',@getParabolicProfilePLUSPLUS);
DuneErosionSettings('set','rcParabolicProfileFcn',@getRcParabolicProfilePLUSPLUS);
DuneErosionSettings('set','invParabolicProfileFcn',@invParabolicProfilePLUSPLUS);
DuneErosionSettings('set','Plus','-plusplus'); % sets D++
DuneErosionSettings('set','d',0);  % Set waterdepth relative to reference level (e.g. in m NAP). In the model the SWL is added to this value to obtain the total water depth.
    
% calculate the erosion profile, erosion volumes etc.
% [result, messages] = DUROS(x, z, D50, WL, Hs, Tp);
dresult = DUROS(x,z,D50,WL,Hs,Tp);
% figure;hold on
% plot(dresult(1,3).xSea,dresult(1,3).zSea)
% plot(dresult(1,3).xActive,dresult(1,3).zActive,'r')
% plot(dresult(1,3).xLand,dresult(1,3).zLand)
% plot(x,z,'g:')

%% make sure input and output extent are the same
zin  = [transp(dresult(1,1).zLand), transp(dresult(1,1).zActive), transp(dresult(1,1).zSea)];
xin  = [transp(dresult(1,1).xLand), transp(dresult(1,1).xActive), transp(dresult(1,1).xSea)];

% dresult(1,1): 'DUROS-plus'
% dresult(1,2): 'DUROS-plus Erosion above SSL'
% dresult(1,3): 'Additional Erosion'
% dresult(1,4): 'Boundary Profile'
% we need Additional Erosion
if size(dresult,2) >= 3
    xout1 = [transp(dresult(1,2).xLand), transp(dresult(1,2).xActive), transp(dresult(1,2).xSea)];
    zout1 = [transp(dresult(1,2).zLand), transp(dresult(1,2).z2Active), transp(dresult(1,2).zSea)];
    xout2 = [transp(dresult(1,3).xLand), transp(dresult(1,3).xActive), transp(dresult(1,3).xSea)];
    zout2 = [transp(dresult(1,3).zLand), transp(dresult(1,3).z2Active), transp(dresult(1,3).zSea)];
% xmin = max(min(xout(:)),min(xin(:)));
% xmax = min(max(xout(:)),max(xin(:)));
% topoin  = zin(find(xin==xmin,1,'first'):find(xin==xmax,1,'last'));
% topoout1  = zout1(find(xout==xmin,1,'first'):find(xout==xmax,1,'last'));
% topoout2  = zout2(find(xout==xmin,1,'first'):find(xout==xmax,1,'last'));

    V1 = jarkus_getVolume(xin, zin, [], 3);
    V2 = jarkus_getVolume(xout1, zout1, [], 3);
    V3 = jarkus_getVolume(xout2, zout2, [], 3);
    r1 = V2-V1;
    r2 = V3-V1;

%% plot the results
    if exist('test_plot','var')
        fig = figure(...
            'Position',[200 200 600 400],...
            'Color','w');
        plotDuneErosion(dresult,fig);
        % 
        figure; plot(xin, zin,'k')
        hold on
        plot(xout1, zout1,'m')
        plot(xout2, zout2,'r')
    end

else r1 = nan; r2 = nan;
end
