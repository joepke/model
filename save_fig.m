function []=save_fig(fig, fn, width_inch, height_inch, res, sq)
%SAVEFIG saves plot window to file
% FIG = SAVE_FIG(FIG, FILENAME, WIDTH[INCH], HEIGHT[INCH], RESOLUTION, SQUARE?)
% fig: handle to figure
% fn: filename
% width: [inch] width of figure
% height: [inch] height of figure
% res: [px/inch] resolution
% sq: [0 | 1] force plots to be square [1]

%% uncomment to test standalone
% fig=figure;
% plot(1:10)
% width = 4;
% height= 4;
% res=300;

%% save figure with defined size and resolution

% option to force plots to be square 
if (sq == 1)
    % Extract axes handles of all subplots from the figure
    axesHandles = findobj(get(fig,'Children'), 'flat','Type','axes');
    % Set the axis property to square
    axis(axesHandles,'square')
end

% Set unit and size of figure
set(fig, 'PaperUnits', 'inches');
x_width=width_inch;y_width=height_inch;
set(fig, 'PaperPosition', [0 0 x_width y_width]); %
resstr = ['-r',num2str(res)];

% Write figure to file
print(fig, '-dtiff',resstr, fn)


