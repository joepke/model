%% perform beachupdate-type erosion on historical data
% water level from West-Terschelling tide records 
% topographical data from JARKUS database
% compare modeled erosion (beachupdate) with measured volume change (JARKUS)
% note that measured change includes aeolian transport prior to and after
% storm surge event

close all

%% MODEL PARAMETERS
prs         = [13:18]; %;20000 9000];   % JARKUS profiles number for which to perform the simulation
ar          = 3;        % JARKUS area (3 = Ameland)
cellsize    = 1;         
slabheight  = 0.1;
dl          = 0.3;      % depth limit
di          = 0.012;    % dissipation strength
wa          = 1;        % wave energy
vegres      = 0;        % resistance of vegetation
pwavemaxf   = 1;        % do not erode/accrete beyond equilibium topography
pwaveminf   = 1;        % always full reset where inundated
shelterf    = 1.0;      % full shelter required to avoid erosion of landward side

if ar == 4 
    load dV_trs.mat
end
if ar == 3
    load dV_aml.mat
end

load sea_aml.mat
time = maxsealevels.time;
[modeled, modeled_DUROS, retreat] = deal(zeros(1,length(time)));
m = 1; n=length(prs);
figure;

for pr = 17
    disp(pr)
    pr = pr*1000;
    p = find(prs==pr, 1,'first');
    measurements = dV.dV(find(dV.along==pr, 1),:);
%     measurements(measurements>0) = nan;
       
    for i = 1:length(time)

        surge = maxsealevels.max(i);
        measured = measurements(:,i);
 
        if ~isnan(measured) %&& measured <0;
        
            try
            [topoin, topoout, eroded_volume, retr] = test_beachupdate(ar, pr, time(i), surge, ... 
                                                 slabheight, cellsize, di, ...
                                                 wa, vegres, pwavemaxf, ...
                                                 pwaveminf, dl, shelterf);

%             [dV1, dV2] = test_DUROS(ar, pr, time(i), surge);
%             modeled_DUROS(i) = dV2;
            modeled(i) = eroded_volume;
%             retreat(i) = retr;
            catch ME
            end
            
        end
                                                     
    end
    
    m=2; n=1;
    p = 1;
    subplot(m,n,p)
    plot(modeled, measurements, 'bo')
    line(xlim, xlim)
    
%     plot(measurements, modeled, 'k*')
%     plot(measurements, modeled_DUROS, 'ro')
%     plot(modeled, modeled_DUROS, 'ro')
%     plot(maxsealevels.max, modeled, 'bo','displayname','model')
%         hold on
%     plot(maxsealevels.max, measurements, 'r*','displayname','observation')
%     for i = 1:length(measurements)
%         line([maxsealevels.max(i) maxsealevels.max(i)], [modeled(i) measurements(i)])
%     end
    %% plot temporal variation in dune erosion
    % measurements (JARKUS)
    p = 2;
    subplot(m,n,p)
    plot(maxsealevels.time, measurements, 'b-o')
    hold on
    % modeled erosion (DUBEVEG)
    plot(maxsealevels.time, modeled, 'r-x')
%     for i = 1:length(measurements)
%         line([maxsealevels.time(i) maxsealevels.time(i)], [modeled(i) measurements(i)])
%     end
    
%     xlabel('measured','interpreter','latex')
%     xlabel('modelled DUBEVEG','interpreter','latex')
%     ylabel('modelled','interpreter','latex')
%     title('dune erosion ($m^3/m$)','interpreter','latex')
%     legend show
% csvwrite(['C:/joep/Dropbox/model/calibration/erosion_predicted',num2str(pr),'.csv'], modeled')
% csvwrite(['C:/joep/Dropbox/model/calibration/erosion_observed',num2str(pr),'.csv'], measurements')

       
end

% save_fig(gcf, 'erosion_pwavemin5.png', 10, 6, 600, 1)