function [r1,r2,r3]=shiftslabs4(erosmap, deposmap, hopmap)
% to do the shifting of the sand
% erosprobs     : map of erosion probabilities [erosmap]
% deposprobs    : map of deposition probabilities [deposmap]
% hop           : = jumplength
% movement is from left to right, along +2 dimension, across columns, along rows
% returns a map of height changes [-,+]
% open boundaries (modification by Alma), no feeding from the sea side
%% input parameters for testing
% [topo, veg] = read_profile_from_netcdf(3,20000,2002,0);
% [topo, eqtopo, spec1at0, spec2at0] = create_topographies(topo, veg);
% 
% hopmap        = zeros(size(topo));
% hopmap(:,2:end)= round(1 + 2*(transp(diff(transp(topo)))));
% hopmap(hopmap<1) = 1;
% veg = spec1at0 + spec2at0;
% topo = topo ./ 0.1;
% sandmap       = topo>0;                                       % returns T(one) for sandy cells
% shadowmap     = shadowzones2(topo, 0.1, 15);  % returns T(one) for in shadow
% gw            = round(eqtopo .* 0.6);           % gw lies under beach with less steep angle
% erosmap     = erosprobs(veg, shadowmap, sandmap, topo, gw); % returns map of erosion probabilities
% deposmap      = depprobs(veg, shadowmap, sandmap, 0.4, 0.6); % returns map of deposition probabilities

pickedup=bsxfun(@lt, rand(size(erosmap)), erosmap);     % T where slab is picked up
totalpickedup = sum(sum(pickedup));
totaldeposit=zeros(size(deposmap));
inmotion=pickedup;   % make copy of original erosion map
numshifted = 0;      % number of shifted cells weighted for transport distance [slabs]
transportdist = 0;   % transport distance [slab lengths] or [hop length]

hop_lengths = (unique(hopmap)).';
% figure(h)
while sum(inmotion(:))>0                    % while still any slabs moving
    for h = hop_lengths
        inmotion_sub = inmotion .* (hopmap==h);
%         disp(['h = ', num2str(h), ' n = ',num2str(sum(inmotion_sub(:)))])
        if sum(inmotion_sub(:)) > 0
%             subplot(3,1,1)
%             imagesc(inmotion_sub)
            inmotion = inmotion - inmotion_sub;
            transportdist = transportdist+1;        % every time in the loop the slaps are transported one length further to the right
            inmotion_sub=circshift(inmotion_sub, [0, h]); % shift the moving slabs one hop length to the right
            depocells=bsxfun(@lt, rand(size(deposmap)), deposmap);  % T where slab should be deposited
            depocells(:,1:h)=1;                   % all slabs are deposited if they are transported over the landward edge
            
            deposited=inmotion_sub.*depocells;          % T where a slab is available and should be deposited
%             subplot(3,1,2)
%             imagesc(deposited)
            inmotion_sub = inmotion_sub - deposited;
            inmotion = inmotion + inmotion_sub;            % left over in transport after this round of deposition
            numshifted = numshifted + sum(deposited(:)).* transportdist;  % number of slabs deposited, weighted for transport distance
            deposited(:,1:h)=0; % remove  all slabs that are transported from the
            totaldeposit = totaldeposit+deposited;    % total slabs deposited so far
            
%             subplot(3,1,3)
%             imagesc(totaldeposit)
%             pause(1)
%             disp(sum(totaldeposit(:)))
        end
    end
    
    
    
        %  landward side to the seaward side (this changes the periodic
        % boundaries into open ones)
  

   
end

r1 =(-pickedup)+totaldeposit;                % -erosion + deposition
r2 = numshifted;                            % total transport of slabs
r3 = totalpickedup;