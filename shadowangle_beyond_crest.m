
sh=0.1;
sa=15;
[z, v] = read_profile_from_netcdf(3,20000,2002,0);
[topo, ~, veg1, veg2] = create_topographies(z, v);
topof = topo./sh;
shaded = shadowzones2(topof, sh, sa);


prof=mean(topo,1);
figure; hold on
plot(prof)
shaded = mean(shaded,1);
x0 = find(shaded>0, 1, 'first');
x1 = find(shaded>0, 1, 'last');
X = [x0:x1 x1:-1:x0];
Y = [prof(x0:x1), zeros(1,numel(x0:x1))+max(prof)];
fill(X,Y,'r')
plot(prof)

