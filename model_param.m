classdef model_param
    
    properties
        pmax = 0;
        pmin = 0;
        d = 0;
        r;
        seq;
    end
    
    methods
        
        function  obj = model_param(pmin, pmax, n)
            if(nargin > 0)
                obj.pmax = pmax;
                obj.pmin = pmin;
                obj.d = n/(2*(n-1))*(pmax-pmin);
                obj.seq = pmin:(obj.d/2):pmax;
                obj.r = obj.seq(randi(n));
            end
        end
        
        function  obj = mp_rand(obj)
            if(nargin > 0)
                obj.r = obj.pmin + (obj.pmax - obj.pmin).*rand(1) + obj.d;
            end
        end
        function val = get_max(obj)
                val = obj.pmax;
        end
        function val = get_delta(obj)
                val = obj.d;
        end
        function val = get_rand(obj)
                val = obj.r;
        end
        function val = get_min(obj)
                val = obj.pmin;
        end
        function val = get_seq(obj)
                val = obj.seq;
        end
    end
    
end