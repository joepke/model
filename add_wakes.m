function [r1]=add_wakes(veg, wakelength)%, waketype)
% Add wakes (zone of reduced wind speed) to vegetation map to simulate the wind-breaking effect of vegetation. 'veg' is model output of vegetation
% efficiency, 'wakelength' is the length of the wake zone in number of cells. The wake is added in cross shore direction by shifting the veg matrix to the right and adding the shifted matrix to the original.

% test standalone
% wakelength=4;
% [topoin, vegin] = read_profile_from_netcdf(3,20000,2002,0);
% [topo, eqtopo, spec1, spec2] = create_topographies(topoin, vegin);
% veg = spec1+spec2;
% veg = zeros(1,100);
% veg(40) = 1;
% veg = repmat(veg,10,1);
% 
% 
veg2 = veg;
if wakelength > 0% dimensions
    longshore = size(veg,1);
    crossshore = size(veg,2);

    % vegetation presence
    % shift vegetation i slabs landward to simulate a wake


    for i=1:wakelength
        dummy = zeros(longshore,i);
        vegshift = [dummy veg(:,1:(crossshore-i))-(1/wakelength)];
        vegshift(vegshift<0) = 0;
        veg2 = veg2+vegshift;
    end
    
    % indicate zonation
%     figure;
%     subplot(2,1,1)
%     axis equal tight;
%     veg1=~(veg==0);
%     wakes = (veg1==0 & ~(veg2==0))*2;
%     vegwakes = wakes+veg1;
%     imagesc(vegwakes);
%     colormap(hsv(3))
%     labels={'bare', 'vegetation', 'wake'};
%     xlabel('cross shore (m)'); ylabel('longshore (m)')
%     axis equal tight;
%     lcolorbar(labels)
%     subplot(2,1,2);
%     hold on
%     plot(mean(veg,1))
%     plot(mean(veg2,1),'r--')
end


r1 = veg2;