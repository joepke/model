function [r1] = find_crest(x, min_crest_height)
   
x = x(:);
upordown = sign(diff(x));
maxflags = [upordown(1)<0; diff(upordown)<0; upordown(end)>0];
maxima   = find(maxflags);
crest    = find(x(maxima)>min_crest_height,1,'first');
crest    = find(x==x(maxima(crest)),1,'first'); 
r1 = crest;