
% read JARKUS source 
z = xlsread('pr20000_yr2000.xls')/100;

% select 50 most landward measurements
z = flipdim(z,1);
z = z(1:51);
x = (0:numel(z)-1)*5;

% interpolate from 5 m to 1 m cross shore resolution
xi = 1:1:250;
yi = interp1(x,z,xi);

% select beach part and fit equilibrium topography
footlevel = 2; % in m + NAP
foot = find(yi>=footlevel,1,'first');
yy = yi(1:foot);
xx = xi(1:foot);
a = (yy(foot)-yy(1))/(xx(foot)-xx(1));
eqy = xi*a;

% create 2D topographies by stacking to matrix
topo   = repmat(yi,50,1);
eqtopo = repmat(eqy,50,1);

% plot results
figure;
plot(xi,yi, 'displayname', 'initial topography');
hold on; 
plot(xi,eqy,'r--','displayname','equilibrium topography');
legend('show','Location','Northwest');
hold off
ylabel('elevation (m)');
xlabel('cross shore (m)');

% save 'topoameland.mat' topo eqtopo
