classdef model_input<handle
    
    properties
        zInitial = [linspace(0,3,100), linspace(3,12,50), linspace(12,6,30), linspace(6,6,20)];
        vInitial = [linspace(0,0,100), linspace(0,0.8,50), linspace(0.8,1,30), linspace(1,1,20)];
        area_code = 3;
        profile_id = 20000;
        start_year = 2002;
        simulation_time_y = 9;
        perturbation_time = -1;
        %aeolian parameters
        iterations_per_cycle = 26;
        slabheight_m = 0.1;
        jumplength = 1;
        groundwater_depth = 0.9;
        p_erosion = 0.5;
        p_deposition_bare = 0.08;
        p_deposition_veg = 0.2;
        shadow_angle = 15;
        repose_bare = 20;
        repose_vegetated = 30
        %marine parameters
        mean_sea_level = 0;
        wave_energy = 1;
        dissipation_rate = 0.012;
        depth_limit = 0.4;
        %vegetation parameters
        sp1_a = -1.2;               
        sp1_b = 0.1;
        sp1_c = 0.5;
        sp1_d = 1.5;
        sp1_e = 2.5;
        sp1_peak = 0.2;
        sp2_a = -1.3;
        sp2_b = -0.65;
        sp2_d = 0.2;
        sp2_e = 2.2;
        sp2_peak = 0.05;
        pioneer_probability = 0.05;
        lateral_probability = 0.2;
        sp1_vigour = 1;
    end
    
    methods
        function fvalues = get_values(OPT)
            fnames = fieldnames(OPT);
            fvalues = zeros(1,length(fnames));
            for i = 1:length(fnames)
                v = getfield(OPT, fnames{i});
                if isscalar(v)
                    fvalues(i) = v;
                end
            end
        end
        
         function OPT = set_value(OPT, fieldname, value)
%              if value 
                 OPT = setfield(OPT, fieldname, value);
%              else
%                  OPT = setfield(OPT, fieldname, );
%              end
         end
    end
            
    
end