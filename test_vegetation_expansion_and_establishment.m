[topo, veg] = read_profile_from_netcdf(3, 17000, 2002,0);
[topof, eqtopo, spec1, spec2] = create_topographies(topo, veg);

slabheightm = 0.1;
figure()

for t = 1:100
%     topof = topof + balance
    balance = rand(size(topof))*0.4-0.2;
    stability = balance;
%     topof = topof + balance; 
    spec1_old = spec1;
    spec2_old = spec2;
    spec1     = growthfunction1_sens(spec1, balance.*slabheightm, -2, 0, 0.4, 1, 2, 0.1);
    spec2     = growthfunction2_sens(spec2, balance.*slabheightm, -1.3, -0.6, 0.2, 2, 0.05);

    lateral1 = lateral_expansion(spec1_old, 1, 0.1);
    lateral2 = lateral_expansion(spec2_old, 1, 0.1);
    pioneer1 = establish_new_vegetation(topof, 0) .* (spec1_old <= 0);
    pioneer2 = (stability==0) .* (spec2_old <= 0);
    % determine changes in vegetation cover 
    spec1_diff = spec1 - spec1_old;
    spec2_diff = spec2 - spec2_old;
    
    
    % split cover changes into into gain and loss
    spec1_growth = spec1_diff .* (spec1_diff > 0);
    spec1_loss = spec1_diff .* (spec1_diff < 0);
        
    spec2_growth  = spec2_diff .* (spec2_diff > 0);
    spec2_loss = spec2_diff .* (spec2_diff < 0);

    % only allow growth in adjacent or pioneer cells
    spec1_growth_allowed = spec1_growth .* (lateral1 | pioneer1);
    spec2_growth_allowed = spec2_growth .* (lateral2 | pioneer2);

    % re-assemble gain and loss and add to original vegetation cover
    spec1 = spec1_old + spec1_growth_allowed + spec1_loss;
    spec2 = spec2_old + spec2_growth_allowed + spec2_loss;
    
    % update topography with aeolian transport
    topof = topof + balance;
       
    % apply beach update
    [topof, inundated, pbeachupdate] = beachupdatecacomnoran_test(15 + rand()*20, 0.1, 1, topof./0.1, eqtopo , spec1 , ...
            0.1, 1, 0, 1, 1, 0, 0.2, 0, 6, 0.5);
    topof = topof.*0.1;    
    pbeachupdate(pbeachupdate<0) = 0;
    pbeachupdate(pbeachupdate>1) = 1;
    
    % remove vegetation affected by beach update
    spec1     = spec1.*(1-pbeachupdate);   % remove species where beach is reset
    spec2     = spec2.*(1-pbeachupdate);   % remove species where beach is reset
    
end
    m = 6; n=1;
    subplot(m,n,1)
    imagesc(topof)
    title('topography')

    subplot(m,n,2)
    imagesc(balance)
    title('erosion/deposition')

    subplot(m,n,3)
    imagesc(spec1)
    title('pioneer species')

    subplot(m,n,4)
    imagesc(spec2)
    title('secondary species')

    subplot(m,n,5)
    imagesc(pioneer1)
    title('growth allowed for pioneer sp.')

    subplot(m,n,6)
    imagesc(lateral1)
    title('growth allowed for secondary sp.')
% end