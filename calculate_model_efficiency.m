function [r1]=calculate_model_efficiency(OBS, SIM, ITEM)
%CALCULATE_MODEL_EFFICIENCY(OBS, SIM) determines Nash Sutcliffe coefficient
% of modeled vs measured data series

% if no OBS exist, ME cannot be calculated
if all(isnan(OBS))
    warning('ME:NODATA','Model efficiency for "%s" cannot be calculated. No observations.', ITEM);
    ME = nan;
else
    
    % flip array from horizontal to vertical if necessary
    if size(OBS) == size(transpose(SIM))
        SIM = transpose(SIM);
        disp(sprintf('SIM array transposed for "%s".', ITEM))
    end
    
    % if OBS has several NA values: omit those in both OBS and SIM
    if ~all(~isnan(OBS))
        SIM = SIM(~isnan(OBS));
        OBS = OBS(~isnan(OBS));
    end
    
    % check if arrays have same length
    if length(OBS) ~= length(SIM) 
        warning('ME:LENGTHERROR','Model efficiency for "%s" cannot be calculated. length(OBS) = %g, while length(SIM) = %g.', ITEM, length(OBS), length(SIM));
        ME = nan;
    else
%         disp(size(OBS))
%         disp(size(SIM))
        % if no problems: calculate Nash Sutcliffe coefficient
        ME = 1 - (sum( (OBS - SIM).^2) / sum( (OBS - mean(OBS)).^2));
    end
end

r1 = ME;
