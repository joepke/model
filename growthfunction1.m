function r=growthfunction1(spec, sed)
% input is the vegetation map, and the sedimentation balance in units of cell dimension (!), i.e.
% already adjusted by the slabheight
% output is the change in vegetation effectiveness
% Ammophilia-like vegetation

% physiological range (needs to be specified)
minimum= 0.0;
maximum= 1.0;

% vertices (these need to be specified)
x1 = -1.4;  y1 = -1.0;    % was x1=-1.4; y1=-1.0;
x2 =  0.1;  y2 =  0.0;    % was x2= 0.1; y2= 0.0;
x3 =  0.45; y3 =  0.4;    % was x3= 0.45; y3= 0.4;
x4 =  0.85; y4 =  0.0;    % was x4= 0.85; y4= 0.0;
x5 =  1.4;  y5 = -1.0;    % was x5= 1.4; y5=-1.0;

% slopes between vertices (calculated from vertices)
s12=(y2-y1)/(x2-x1);
s23=(y3-y2)/(x3-x2);
s34=(y4-y3)/(x4-x3);
s45=(y5-y4)/(x5-x4);

leftextension=bsxfun(@lt, sed, x1)                   .* -1;
firstleg =bsxfun(@ge, sed, x1).*bsxfun(@lt, sed, x2) .* ( (sed-x1)*s12 + y1);
secondleg=bsxfun(@ge, sed, x2).*bsxfun(@lt, sed, x3) .* ( (sed-x2)*s23 + y2);
thirdleg =bsxfun(@ge, sed, x3).*bsxfun(@lt, sed, x4) .* ( (sed-x3)*s34 + y3);
fourthleg=bsxfun(@ge, sed, x4).*bsxfun(@lt, sed, x5) .* ( (sed-x4)*s45 + y4);
rightextension=bsxfun(@ge, sed, x5)                  .* -1;

spec=spec+leftextension+firstleg+secondleg+thirdleg+fourthleg+rightextension;

spec(spec < minimum) = minimum; 
spec(spec > maximum) = maximum;

r=spec;