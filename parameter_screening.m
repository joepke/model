% parameter screening
clear all
close all

% initialize random number generator
rand('state', 1234)

% simulation settings
area_code = 3;
start_year = 2002;
simulation_time_y = 9;

for profile_id = [20000]  
    
    % get model input
    OPT = model_input;
    fname = fieldnames(OPT);
    fname = fname(7:end);
    nf = numel(fname);
    % perform 10 perturbation trajectories
    nr = 1;
    % simulation of 9 years
    ny = 9;
    allins = nan(nr, nf+1, nf);
    
    OPT.profile_id = profile_id;
    OPT.area_code =  area_code;
    OPT.start_year = start_year;
    OPT.simulation_time_y = simulation_time_y;
    close all


    % create empty matrices for storing elementary effects
    [EEvol, EEprof, EEveg, dQv, dQm, dV] = deal(nan(nf, nr));
    % create empty matrices for storing performance indices
    [NSEvol, NSEprof, NSEveg, Qv, Qm, V] = deal(nan(nf+1, nr));


    for r = 1:nr

        X = sprintf('trajectory %i of %i', r, nr);
        disp(X)

        %% define realistic range for each parameter
        % aeolian
        iterations_per_cycle    = model_param(26, 52, 4);
        slabheight_m            = model_param(0.05, 0.2, 4);
        jumplength              = model_param(1, 10, 4);
        groundwater_depth       = model_param(0.0, 1.0, 4);
        p_erosion               = model_param(0.1, 1.0, 4);
        p_dep_bare              = model_param(0.1, 0.9, 4);
        p_dep_sand              = model_param(0.1, 0.9, 4);
        shadow_angle            = model_param(5, 45, 4);
        repose_bare             = model_param(20, 50, 4);
        repose_vegetated        = model_param(20, 50, 4);
        mean_sea_level          = model_param(-1, 1, 4); 
        wave_energy             = model_param(1, 10, 4);
        dissipation_rate        = model_param(0.01, 0.02, 4);
        depth_limit             = model_param(0.1, 1, 4);
        runup_a                 = model_param(0, 200, 4);
        sp1_a                   = model_param(-1.5, -0.5, 4);
        sp1_b                   = model_param(0.00, 0.25, 4);
        sp1_c                   = model_param(0.3, 0.9, 4);
        sp1_d                   = model_param(1, 2, 4);
        sp1_e                   = model_param(2.2, 3, 4);
        sp1_peak                = model_param(0.0, 1.0, 4);
        sp1_lateral             = model_param(0.0, 1.0, 4);
        sp2_a                   = model_param(-2, -1.2, 4);
        sp2_b                   = model_param(-1, -0.5, 4);
        sp2_d                   = model_param(0.2, 1, 4);
        sp2_e                   = model_param(2, 3, 4);
        sp2_peak                = model_param(0.0, 1.0, 4);
        sp2_lateral             = model_param(0.0, 1.0, 4);

        %% create cell array of deltas (d) and initial input (ins)
        [pmax, pmin, d,  ins, prange] = deal(cell(1,nf));
        for nn = 1:nf
            pmax{nn} = get_max(eval(fname{nn}));
            pmin{nn} = get_min(eval(fname{nn}));
            d{nn}    = get_delta(eval(fname{nn}));
            random_value  = get_rand(eval(fname{nn}));
            ins{nn}  = random_value;
            OPT = set_value(OPT, fname{nn}, random_value);
            prange{nn}   = pmax{nn} - pmin{nn};
        end
        allins(r, 1, :) = [ins{:}];

        %% first run
        [nsevol, nseprof, nseveg] = model_sens(OPT);
        NSEvol(1,r) = nsevol; 
        NSEprof(1,r) = nseprof; 
        NSEveg(1,r) = nseveg; 


        %% perform runs with small perturbation in a factor, one at a time, in random order
        % generate random order of perturbations
        ord = randperm(nf);
        nameord = fname(ord);

        for i=1:1

            close all
            j = ord(i);
            X = sprintf('factor %i of %i, changing "%s"', i, nf, fname{j});
            disp(X)

            % copy model performance of last run so we can compare it
            % against the next run
            nsevol_old = nsevol;
            nseprof_old = nseprof;
            nseveg_old  = nseveg;
            ins_old = ins;

            disp(['before:', fname{j}, ' = ', num2str(ins{j})])

            %% now perturb one parameter value by delta_p <d>
            ins{j} = ins{j} + d{j};
            if ins{j} > pmax{j}
                ins{j} = ins{j} - 2*d{j};
            end

            disp(['after: ', fname{j}, ' = ', num2str(ins{j})])

            % write changed value to model_input object
            OPT = set_value(OPT, fname{j}, ins{j});
            allins(r, i+1, :) = [ins{:}];

            % run model with perturbed input and retrieve results
            [nsevol, nseprof, nseveg] = model_sens(OPT);
            NSEvol(i+1,r) = nsevol;
            NSEprof(i+1,r) = nseprof;
            NSEveg(i+1,r) = nseveg;
            X = sprintf('ME_vol: %g, ME_dz: %g, ME_veg: %g', nsevol, nseprof, nseveg);
            disp (X)

            % calculate relative change in model performance
            % a.k.a. Elementary Effect
            EEvol(j,r)=  (nsevol-nsevol_old) / ((ins{j}-ins_old{j})/prange{j});
            EEprof(j,r)= (nseprof-nseprof_old) / ((ins{j}-ins_old{j})/prange{j});
            EEveg(j,r)= (nseveg-nseveg_old) / ((ins{j}-ins_old{j})/prange{j});

        end % parameter
    end % run
end % profile

% pr =20000; sy=9;

fn = ['./temp/new_screening_',num2str(profile_id),'_',num2str(start_year),'_1.mat'];
if exist(fn,'file')
    warning('WARNING:OUTPUT_FILE_EXISTS','Output filename already exist. Generating new name.')
    id = str2num(fn(end-4));
    newid=id+1;
    disp(id)
    disp(newid)
else
    newid = 1;
end

% store workspace
fn = ['./temp/new_screening_',num2str(profile_id),'_',num2str(start_year),'_',num2str(newid),'.mat'];
save(fn)

% write Elementary Effects to tab-separated txt file
items = {'EEvol', 'EEprof','EEveg'};
for item = items
    M = eval(item{:});
    L = fname;
    header  = [sprintf('%s\t',L{1:end-1}), L{end}];
    fn = ['./temp/new_screening_', item{:}, '_', num2str(profile_id),'_',num2str(start_year),'_',num2str(newid),'.txt'];
    fid = fopen(fn, 'wt');
    fprintf(fid, '%s \n', header);
    dlmwrite(fn, transpose(M), 'delimiter', '\t', '-append')
    fclose(fid);
end