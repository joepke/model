function [r1,r2,r3,r4,r5,r6]=shiftslabs3(erosprobs, deposprobs, hop)
% to do the shifting of the sand
% erosprobs     : map of erosion probabilities [erosmap]
% deposprobs    : map of deposition probabilities [deposmap]
% hop           : = jumplength
% movement is from left to right, along +2 dimension, across columns, along rows
% returns a map of height changes [-,+]
% open boundaries (modification by Alma), no feeding from the sea side

pickedup=bsxfun(@lt, rand(size(erosprobs)), erosprobs);     % T where slab is picked up
totalpickedup = sum(sum(pickedup));
totaldeposit=zeros(size(deposprobs));
inmotion=pickedup;   % make copy of original erosion map
numshifted = 0;      % number of shifted cells weighted for transport distance [slabs]
transportdist = 0;   % transport distance [slab lengths] or [hop length]
shifted_landwards = 0;
while sum(inmotion(:))>0                    % while still any slabs moving
    transportdist = transportdist+1;        % every time in the loop the slaps are transported one length further to the right
    inmotion=circshift(inmotion, [0, hop]); % shift the moving slabs one hop length to the right
    depocells=bsxfun(@lt, rand(size(deposprobs)), deposprobs);  % T where slab should be deposited
    depocells(:,1:hop)=1;                   % all slabs are deposited if they are transported over the landward edge
    deposited=inmotion.*depocells;          % T where a slab is available and should be deposited
    inmotion=inmotion-deposited;            % left over in transport after this round of deposition
    numshifted = numshifted + sum(deposited(:)).* transportdist;  % number of slabs deposited, weighted for transport distance
    landwards = deposited(:,1:hop);
    shifted_landwards = shifted_landwards + sum(landwards(:));
    deposited(:,1:hop)=0; % remove  all slabs that are transported from the
    %  landward side to the seaward side (this changes the periodic
    % boundaries into open ones)
    totaldeposit=totaldeposit+deposited;    % total slabs deposited so far
end

cross_shore_ero =  mean(pickedup,1);
cross_shore_dep =  mean(totaldeposit,1);

r1 =(-pickedup)+totaldeposit;                % -erosion + deposition
r2 = numshifted;                            % total transport of slabs
r3 = totalpickedup;
r4 = cross_shore_ero;
r5 = cross_shore_dep;
r6 = shifted_landwards;