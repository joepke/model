% species 1
figure;
hold on

x1 = -1.4;  y1 = -1.0;    % was x1=-1.4; y1=-1.0;
x2 =  0.1;  y2 =  0.0;    % was x2= 0.1; y2= 0.0;
x3 =  0.45; y3 =  0.4;    % was x3= 0.45; y3= 0.4;
x4 =  0.85; y4 =  0.0;    % was x4= 0.85; y4= 0.0;
x5 =  1.4;  y5 = -1.0;    % was x5= 1.4; y5=-1.0;
X1 = [x1, x2, x3, x4, x5]; Y1 = [y1, y2, y3, y4, y5];

% sensitivity bounds
bx1       = model_param(-0.1, 0.15, 4);
cx1       = model_param(-0.15, 0.25, 4);
dx1       = model_param(0, 0.5, 4);
cy1       = model_param(-0.25, 0.25, 4);


% B
rectangle('Position',[x2+get_min(bx1),-0.01, get_max(bx1)-get_min(bx1),0.02],...
    'EdgeColor','none','FaceColor',[0.8 0.8 0.8])
% C
rectangle('Position',[x3+get_min(cx1),y3+get_min(cy1), get_max(cx1)-get_min(cx1),get_max(cy1)-get_min(cy1)], ...
    'EdgeColor','none','FaceColor',[0.8 0.8 0.8])
% D
rectangle('Position',[x4+get_min(dx1),-0.01, get_max(dx1)-get_min(dx1),0.02],...
    'EdgeColor','none','FaceColor',[0.8 0.8 0.8])

vertex = {'A', 'B', 'C', 'D', 'E'};
for ii = 1:numel(X1) 
    text(X1(ii), Y1(ii),vertex{ii},'FontSize',8,'VerticalAlignment','bottom', ...
                         'HorizontalAlignment','right','BackgroundColor',[0.6 0.6 0.6]) 
end

plot(X1, Y1, 'b*-', 'DisplayName','sp. 1')
legend show
x = get(gca,'XLim');
y = get(gca,'YLim');
plot([0, 0], [y(1) y(2)], 'k--') 
plot([x(1), x(2)] ,[0, 0], 'k--') 




%% species 2
% create figure
figure;
hold on

% vertices
x1 = -1.3;  y1 = -1.0;   % was x1=-1.3; y1=-1.0;
x2 = -0.65; y2 =  0.0;   % was x2=-0.65; y2= 0.0;
x3 =  0.0;  y3 =  0.1;   % was x3= 0.0; y3= 0.1;
x4 =  0.2;  y4 =  0.0;   % was x4= 0.2; y4= 0.0;
x5 =  2.2;  y5 = -1.0;   % was x5= 2.2; y5=-1.0;
X2 = [x1, x2, x3, x4, x5]; Y2 = [y1, y2, y3, y4, y5];

% sensitivity bounds
bx2       = model_param(-0.6, 0.2, 4);
cx2       = model_param(-0.15, 0.15, 4);
dx2       = model_param(0, 1.8, 4);
cy2       = model_param(-0.05, 0.45, 4);

% draw rectangles
% B
rectangle('Position',[x2+get_min(bx2),-0.01, get_max(bx2)-get_min(bx2),0.02],...
    'EdgeColor','none','FaceColor',[0.8 0.8 0.8])
% C
rectangle('Position',[x3+get_min(cx2),y3+get_min(cy2), get_max(cx2)-get_min(cx2),get_max(cy2)-get_min(cy2)], ...
    'EdgeColor','none','FaceColor',[0.8 0.8 0.8])
% D
rectangle('Position',[x4+get_min(dx2),-0.01, get_max(dx2)-get_min(dx2),0.02],...
    'EdgeColor','none','FaceColor',[0.8 0.8 0.8])
vertex = {'A', 'B', 'C', 'D', 'E'};

% add vertex labels
for ii = 1:numel(X2) 
    text(X2(ii), Y2(ii),vertex{ii},'FontSize',8,'VerticalAlignment','bottom', ...
                         'HorizontalAlignment','right','BackgroundColor',[0.6 0.6 0.6]) 
end

plot(X2, Y2, 'r*-', 'DisplayName','sp. 2')
legend show
x = get(gca,'XLim');
y = get(gca,'YLim');

plot([0, 0], [y(1) y(2)], 'k--') 
plot([x(1), x(2)] ,[0, 0], 'k--') 


%% surrogate growth function species 1
figure
hold on

xb = 0.05+0.2*rand;
xa = -1.5 + rand;
xc = 0.3 + 0.6*rand;
xd = 1.0 + rand;
xe = 2 + rand;
yc = 0.2+ 0.5*rand;

x2 = xb;        y2 = 0;
x1 = xa;        y1 = -1;
x3 = xc;        y3 = yc;
x4 = xd;        y4 = 0; 
x5 = xe;        y5 = -1;

X1n = [x1, x2, x3, x4, x5]; Y1n = [y1, y2, y3, y4, y5];
plot(X1, Y1, 'r*-', 'DIsplayName','original')
plot(X1n, Y1n, 'b*-','DisplayName','synthetic')

legend show
x = get(gca,'XLim');
y = get(gca,'YLim');
plot([0, 0], [y(1) y(2)], 'k--') 
plot([x(1), x(2)] ,[0, 0], 'k--')

%% surrogate growth function species 2
figure
hold on

xa = -2 + rand;
xb = -1 + 0.5* rand;
xd = rand;
xe = 1+2*rand;
yc = 0.05+0.2*rand;


x2 = xb;        y2 = 0;
x1 = xa;        y1 = -1;
x3 = 0;         y3 = yc;
x4 = xd;        y4 = 0; 
x5 = xe;        y5 = -1;

X2n = [x1, x2, x3, x4, x5]; Y2n = [y1, y2, y3, y4, y5];
plot(X2, Y2, 'r*-', 'DIsplayName','original')
plot(X2n, Y2n, 'b*-','DisplayName','synthetic')

legend show
x = get(gca,'XLim');
y = get(gca,'YLim');
plot([0, 0], [y(1) y(2)], 'k--') 
plot([x(1), x(2)] ,[0, 0], 'k--')
