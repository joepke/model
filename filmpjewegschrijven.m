function [] = write_movie(fn, M)
% filmpjewegschrijven.m
% aangepast nav Andreas

% filmpje=film;
% fn = 'sim_2002-2011_17.avi';
mov = avifile(fn, 'fps', 5, 'quality',100, 'compression','none');  
mov = VideoWriter([fn,'.avi'], 'FrameRate', 5);
% fps was 10
% movie file is now created and opened
% making it at slow-motion
% change name at beginning to avoid overwriting

%% settings
startFrame=1;
endFrame=size(M,2);  % should be number of frames in Matlab movie (see workspace)

%% fill movie with recorded frame
for frameNumber=startFrame:endFrame
    mov = addframe(mov,M(frameNumber));
    close
end

mov = close(mov);  % movie file is closed again