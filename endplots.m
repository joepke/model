%% create plots of final result
h=figure('visible','on', 'units', 'normalized', 'outerposition', [0.1, 0.1, 0.9, 0.9]);
%% read measurements
jarkus = read_profile_from_netcdf(area_code, profile_id, start_year, simulation_time_y);

%% final vegetation

% figure(h);
subplot(4,2,1); 
surf(topo*slabheight); shading interp; axis equal;
% imagesc(vegmap); axis tight;
% title('presence/absence vegetation species)')
colorbar

%% final vegetation cover spec 1 and 2
subplot(4,2,4); 
% plot(mean(spec1_geom,1),'k--')
% plot(mean(spec2_geom,1),'r--', 'displayname','Sp. 2')
plot(veg_mod,'k','linewidth',1,  'displayname','Model')
hold on
if exist('veg_obs','var')
    plot(veg_obs,'r','linewidth',1, 'displayname','Out')
end
axis tight;
% plot(vegin,'b','linewidth',1, 'displayname','In')

vegmap = (spec1 > 0) + 10 * (spec2>0);
% imagesc(vegmap); axis tight;
title('presence/absence vegetation species)')
% legend('location','Northwest')
% ylim([0 1])
% xlim([0 size(topos,1)])
% title('veg. cover');
% set(gca,'FontSize',6)
%% cross-shore sedimentation pattern
subplot(4,2,3); hold on;
% for i=2:(size(topos,2))
%     plot(topos(:,i)-topos(:,i-1))
% end
% plot((topos(:,end)-topos(:,1))/size(topos,2),'k','linewidth',2'); title('sedimentation (m/y)')
% ylim([-1.5,1.5])
set(gca,'ColorOrder',jet(size(topos,2)))
plot(vegs)
axis tight;

%% profile development of model
subplot(4,2,6); hold on
set(gca,'ColorOrder',jet(size(topos,2)))
topos( :, all( isnan( jarkus ), 1 ) ) = []; 
plot(topos,'Displayname',sprintf('y %g',1:6)); title('profile development')
ylim([0,20])
xlim([0 size(topos,1)])
hold on
plot(mean(gw,1).*slabheight_m,'k--')

%% model run settings
subplot(4,2,2); 
%fname = {'vc', 'sh', 'sl', 'jl', 'wl','gr', 'pb', 'ps', 'wa', 'di', 'dl', 'sa', 'rb', 'rv'};

xlim([0 15])
ylim([0 10])
axis off;
cellNames = fieldnames(OPT);
cellData = get_values(OPT);
for i = 1:length(cellNames(4:end))
    t1 = text(mod(i,3)*5, 10-floor(i/3), sprintf('%s : ', cellNames{4+i-1}),  'fontsize',8);
    t2 = text(mod(i,3)*5+4, 10-floor(i/3), sprintf('%g', cellData(4+i-1)),  'fontsize',8);
    set(t1,'interpreter','none')
    set(t2,'interpreter','none')
%     text(1+6*floor(i/10),10-i,sprintf('%s', (cellNames{4+i-1})))
%     text(4+6*floor(i/10),10-i,sprintf('%g', cellData{4+i-1}))
end

% text(1,11,sprintf('vc = %i, sh= %3.2f, sl = %3.2f, jl = %i, wl = %i \n gr = %2.1f, pb = %2.1f, ps = %2.1f, wa = %2.1f, di = %4.3f \n dl = %3.2f, sa = %3.1f, rb = %3.1f, rv = %3.2f, \n a1 = %3.2f, b1 = %3.2f, c1 = %3.2f, d1 = %3.2f \n e1 = %3.2f, p1 = %3.2f  a2 = %3.2f, b2 = %3.2f \n d2 = %3.2f, e2 = %3.2f, p2 = %3.2f',...
%     vegc, slabh, mht, hopl, wakelen, gwred, pdepb, pdeps, wavee, dissi, depthl, shadowa, reposeb, reposev, a1, b1, c1, d1, e1, p1, a2, b2, d2, e2, p2), 'fontsize',9)
                   
% text(1,11,['simy = ', num2str(simy)], 'fontsize',9)
% text(1,6,['vegc = ', num2str(vegc)], 'fontsize',9)
% text(1,1,['slabh = ', num2str(slabh)], 'fontsize',9)
% text(11,11,['mht = ', num2str(mht)], 'fontsize',9)
% text(11,6,['wake = ', num2str(wakelen)], 'fontsize',9)
% text(11,1,['prof = ', num2str(profile)], 'fontsize',9)
% text(21,1,['jump = ', num2str(jumplength)], 'fontsize',9)
% text(21,6,['starty = ', num2str(startyear)], 'fontsize',9)


% ylim([0 12])
% % xlim([0 40])
% axis off;

% %% trend in dune volume
% V_modeled = V-V0;
% V_measured =  jarkus >= 3;
% V_measured = V_measured.*(jarkus-3);
% V_measured = sum(V_measured,1);
% V_measured = V_measured .* all( ~isnan( jarkus ), 1 );
% V_measured = V_measured - V_measured(1);
% 
% 
% V_mod = [0 , V_modeled(vegetationcycle:vegetationcycle:simyears*vegetationcycle)];
% success_V = 1 - (rms(V_mod - V_measured)/mean(V_measured));
subplot(4,2,5); plot((0:iterations)/vegetationcycle, [V0, V],'b-');
hold on
plot(0: simyears, V_obs,'k*')
% csvwrite('D:/Dropbox/pres_icar/V_jar_17.csv',transp(V_jar))
plot(0: simyears, V_mod,'bo')
% csvwrite('D:/Dropbox/pres_icar/V_mod_17.csv',transp(V_mod))
title ('dune volume (m^3/m)'); 
xlabel('year');
axis tight;

% linear regression of dune volume
p = polyfit(time/vegetationcycle,V-V_obs(1),1);
% r = p(1) .* time/vegetationcycle + p(2); % compute a new vector r that has matching datapoints in x
txt = [num2str(p(1),2),' m^3/m/y'];
a = axis;
wdth = a(2)-a(1);
ht = a(4)-a(3);
pos = [a(1)+0.05*wdth a(4)-0.08*ht];
text(pos(1),pos(2),txt);
% hold on;
% plot(time/vegetationcycle, r, 'r-');
% hold off;
        
%% profile development of jarkus
% topos( :, all( isnan( jarkus ), 1 ) ) = []; 
% jarkus( :, all( isnan( jarkus ), 1 ) ) = []; 
subplot(4,2,8); hold on
set(gca,'ColorOrder',jet(size(jarkus,2)))
plot(jarkus); title('measured')
ylim([0,20])
xlim([0 size(topos,1)])
xlabel('cross-shore distance (m)')
ylabel('elevation (m)')


%% compare sedimentation profiles
% read measured results
% dz_model  = topos(:,size(topos,2)) - topos(:,1);
% dz_jarkus = jarkus(:,size(jarkus,2)) - jarkus(:,1);

% plot modeled and measured dz
subplot(4,2,7)
% dz_mod = dz_model(~isnan(dz_jarkus));
% dz_jar = dz_jarkus(~isnan(dz_jarkus));
% 
% dz_mod = dz_mod(1:5:end);
% dz_jar = dz_jar(1:5:end);
% 
% corr_dz = corr(dz_mod,dz_jar);
% success_z = corr_dz;
% success_tot = (success_z + success_V)/2;

hold on
plot(dz_obs_5m,'k','displayname','Jark.');  
plot(dz_mod_5m, 'b','displayname','Mod.');
text(3,3,sprintf('MEvol = %3.2f, MEprof = %3.2f, MEveg = %3.2f', ME_V, ME_dz, ME_C),'fontsize',6);
% text(63,3,['dV = ',num2str(R2dV)],'fontsize',6);
% text(123,3,['NSE = ',num2str(NSE)],'fontsize',6);
%hL = legend('location','southwest');
%set(hL,'FontSize',6);

% fn1 = ['pr_',num2str(profile),'_start_', num2str(startyear),'_simy_', num2str(simy), '_vc_', num2str(vegc), '_sh_',num2str(slabh), '_mht_', num2str(mht), '_hopl_', num2str(hopl), '_wake_', num2str(wakelen), '_type_', num2str(waketype)];
% fn = [fn1, '.png'];
% fn = sprintf('D:/Dropbox/model/fig/calibration/c10_%i_%i_vc_%i_sh_%3.2f_sl_%2.1f_jl_%i_wl_%i_gr_%2.1f_pb_%2.1f_ps_%2.1f_wa_%2.1f_di_%4.3f_dl_%3.2f_sa_%3.1f_rb_%3.2f_rv_%3.2f_a1_%3.2f_b1_%3.2f_c1_%3.2f_d1_%3.2f_e1_%3.2f_p1_%3.2f_a2_%3.2f_b2_%3.2f_d2_%3.2f_e2_%3.2f_p2_%3.2f.png', ...
%     profile, startyear, vegc, slabh, mht, hopl, wakelen, gwred, pdepb, pdeps, wavee, dissi, depthl, shadowa, reposeb, reposev, a1, b1, c1, d1, e1, p1, a2, b2, d2, e2, p2);
% fn = sprintf('D:/Dropbox/model/fig/sims/%i_%i_vegred_%i_vc_%i_sh_%3.2f_sl_%2.1f_jl_%i_wl_%i_gr_%2.1f_pb_%2.1f_ps_%2.1f_wa_%2.1f_di_%4.3f_dl_%3.2f_sa_%3.1f_rb_%3.2f_rv_%3.2f_a1_%3.2f_b1_%3.2f_c1_%3.2f_d1_%3.2f_e1_%3.2f_p1_%3.2f_a2_%3.2f_b2_%3.2f_d2_%3.2f_e2_%3.2f_p2_%3.2f.png', ...
%     profile, startyear, grred, vegc, slabh, mht, hopl, wakelen, gwred, pdepb, pdeps, wavee, dissi, depthl, shadowa, reposeb, reposev, a1, b1, c1, d1, e1, p1, a2, b2, d2, e2, p2);
% fn = sprintf('D:/Dropbox/model/calibration/%i_%i_vc_%i_sl_%3.2f.png', profile, startyear, vegc, mht);

% fn = sprintf('C:/joep/Dropbox/model/calibration/%i_%i_vc_%i_sl_%3.2f_yi_%3.2f.png', ...
%     profile, startyear, vegc, mht, yincr);
% fn =  sprintf('C:/joep/Dropbox/model/fig/sims/morph_tests_%i_%i_jl_%i_wl_%i.png', profile, startyear, hopl, wakelen);
% saveas(h,fn,'png')
