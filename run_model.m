west  = 3000:200:5000;
middle = 9000:200:11000;
east = 19000:200:21000;
prs = [middle, east];

trends = nan(1,numel(prs));


close all
%% run dune simulation model (model.m) with parameters specified below
%clear all; close all;

% parameters
pr = 20000;
startyear = 1990;
simyears = 10;
vegcycle = 100;
slabheight = 0.1;
mht = 0.8;

wakelength = 1;
jumplength = 1;
corrs=nan(10,10);
waketype=1;

jumplengths = 1:5;
x = 1:numel(jumplengths);
prs = 20000:200:20000;

wakelengths = 1:5;
y = 1:numel(wakelengths);
corrs=nan(max(y),max(x));
for it1=y
    wakelength = wakelengths(it1);
    for it2 = x
        jumplength = jumplengths(it2);
    % function [r1, r2, r3, r4, r5] = model(simy,     vegc,   slabh,  mht,    hopl,   wakelen, profile)
        [topos, V, veg, trend, endtopo, success] = model(startyear, simyears, round(vegcycle/jumplength), slabheight, mht, jumplength, wakelength, pr, waketype);
        corrs(it1, it2) = success;
    end
end


% V_mod = [0 , V(vegcycle:vegcycle:simyears*vegcycle)];
% jarkus = read_profile_from_netcdf(pr,startyear,simyears);
% V_measured =  jarkus >= 3;
% V_measured = V_measured.*(jarkus-3);
% V_measured = sum(V_measured,1);
% V_measured = V_measured .* all( ~isnan( jarkus ), 1 );
% V_measured = V_measured - V_measured(1);


% figure;hold on; 
% m4 = 150;
% m5 = 2;
% % topo0 = 0:15; 
% for m5 = 0.5
%     red = 1./(1 + m4.*exp(-m5.*topo0));
%     plot(topo0,red,'r')
% end
