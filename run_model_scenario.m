%% run dune model, supplied with a climate scenarios and parameters specified below
% profile on
%% settings
clear all; close all;
rand('state', 1234)

% parameters
% number of runs:
number_of_runs = 1;
simulation_time_y = 99;
start_year = 2002;

% profiles and areacodes for runs
ars = [3     3     4     3    ];
prs = [17000 20000 22000 17800];
sls = [0.0   0.0   0.0   0.0  ];

%% run for four profiles
for i = [2]
    
    area_code = ars(i);
    profile_id = prs(i);
    sl = sls(i);

    for SLR = [0.5]
              
        for VGR = [0]
            
            for r = 1:number_of_runs
%                 r = 10;
                run_number = r + 10;
                rand('state', run_number)
                close all
                X = ['profile: ', num2str(profile_id),'; SLR: ', num2str(SLR),'; VGR: ',num2str(VGR),'; run #: ',num2str(r)];
                disp(X)
                
                OPT = model_input;
                set_value(OPT, 'profile_id', profile_id);
                set_value(OPT, 'area_code', area_code);
                set_value(OPT, 'simulation_time_y', simulation_time_y);
                set_value(OPT, 'repose_bare', 30);
                set_value(OPT, 'repose_vegetated', 35);
                set_value(OPT, 'p_deposition_bare', 0.13);
                set_value(OPT, 'p_deposition_veg', 0.3);
                set_value(OPT, 'iterations_per_cycle', 52);
                set_value(OPT, 'sp1_b', 0.05);
                set_value(OPT, 'depth_limit', 0.4);
                set_value(OPT, 'dissipation_rate', 0.013);
                set_value(OPT, 'perturbation_time', [25, 50, 75]);
             
                OUT = model_scenario(OPT, SLR, VGR, run_number);
                OUT.run = run_number;
                
                %% write output
%                 fn = ['/home/joep/Dropbox/model/scenarios/standard/scenario_',num2str(simulation_time_y),'y_', num2str(profile_id),'_',num2str(area_code),'_SLR_',num2str(SLR, '%+03.1f'),'_VGR_',num2str(VGR, '%+04.0f'),'_', num2str(run_number, '%02.0f'),'.mat'];
%                 fn = ['./temp/scenario_test_',num2str(simulation_time_y),'y_', num2str(profile_id),'_',num2str(area_code),'_SLR_',num2str(SLR, '%+03.1f'),'_VGR_',num2str(VGR, '%+04.0f'),'_', num2str(run_number),'.mat'];
                fn = ['./temp/perturbation_25_50_75_',num2str(simulation_time_y),'y_', num2str(profile_id),'_',num2str(area_code),'_SLR_',num2str(SLR, '%+03.1f'),'_VGR_',num2str(VGR, '%+04.0f'),'_', num2str(run_number),'.mat']
                save(fn, 'OUT')
   

            end % run number
        end % VGR
    end % SLR


end % profile
% profile off
% profile viewer

