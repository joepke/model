function [OUT] = model_scenario(OPT, sea_level_rise, vegetation_growth_reduction, run_number)
%% run model scenaro
% execute a DUBEVEG run with a given increase in sea level and a given
% change in vegetation growth rate.
% OPT                         : the model configuration structure
% sea_level_rise              : amount of sea-level rise between (m)
% vegetation_growth_reduction : reduction or increase in vegetation growth (%)     
% run_number                  : number of run (in 1-50)

do_endplot = 0;
do_yearly_plot = 1;
do_movie = 1;

% display model configuration
disp('Initializing ... ')
disp(OPT)

% Unpack input variables because we get tired of using OPT.<variable>
fields = fieldnames(OPT, '-full');
numberOfFields = length(fields);

for f = 1 : numberOfFields
    thisField = fields{f};
    commandLine = sprintf('%s = OPT.%s', thisField, thisField);
    evalc(commandLine);
end

SLR = sea_level_rise; % total amount of sea level rise in modeled period [m]
VGR  = vegetation_growth_reduction; % growth reduction by end of period [%]
PBT = perturbation_time;
% parameters that need to be integer or rounded:
simy                        = round(simulation_time_y);
iterations_per_cycle        = round(iterations_per_cycle);
slabheight                  = round(slabheight_m*100)/100;
nearshore_slope = 0.01; % profile 20 in 2002: 0.00835981 

if (sp1_a<sp1_b<sp1_c<sp1_d<sp1_e && sp2_a<sp2_b<sp2_d<sp1_e) == 0
    error('Growth function error: need increasing x-values')
end

%% TIME AND SPACE SETTINGS & LOADING INITIAL MAPS
% iterations_per_cycle = round(vegc/ (hopl/1) / (slabh/0.1) / (0.6/pdeps));   % cycle for updating vegetation response: this determines how long a year is [iterations/year]
beachreset   = round(0:(iterations_per_cycle/26):iterations_per_cycle*simulation_time_y);        % how often to reset the wet beach part [iterarions] Should correspond to appr 2 weeks

% topography settings 
% (already used for creating initial matrices to be loaded)
cellsize    = 1;                    % interpreted cell size [m]

% read JARKUS profile from NetCDF file
[topoin, vegin] = read_profile_from_netcdf(area_code, profile_id, start_year,0);

% create required topographies and initial vegetation map
[topo, eqtopo, spec1at0, spec2at0] = create_topographies(topoin, vegin);
eq_slope = (max(eqtopo(:))-min(eqtopo(:)))/size(eqtopo,2);

% equilibrium topography
% load          eqtopo01x250;                  % get equilibrium beach (= part of topo0) [m]
eqtopo0  = round(eqtopo./slabheight_m);     % transform from m into number of slabs
topo0    = round(topo./slabheight_m); 

% load synthetic water levels
if simulation_time_y == 49
    fn = ['./scenarios/simulated_tides_2002_2050_',sprintf('%2.2i',run_number),'.mat'];
end

if simulation_time_y == 99
    fn = ['./scenarios/simulated_tides_2002_2100_',sprintf('%2.2i',run_number),'.mat'];
end
load(fn) % adds synthethic timeseries of tides <simulated_tides> to workspace

% add linear sea-level trend to water level
sea_level_trend_m = transp(linspace(mean_sea_level, SLR, (simulation_time_y*26)));
sea_level_trend = sea_level_trend_m ./ slabheight_m;
tide_levels_m = simulated_tides + sea_level_trend_m;       %[m]
% figure; plot(tide_levels_m)
tide_levels  = (tide_levels_m)./slabheight_m;           %[slabs]

% timeseries of reduction in vegetation growth
growth_reduction_timeseries = linspace(0, VGR/100, simulation_time_y);
    
% dimensions
crossshore = size(topo0, 2) * cellsize;    % cross-shore size of topography [m]
longshore = size(topo0, 1) * cellsize;    % cross-shore size of topography [m]
x = (cellsize:cellsize:ceil(crossshore)); % shore normal axis for plotting. 
y = (cellsize:cellsize:ceil(longshore));  % shore parallel axis for plotting

%% MODEL PARAMETERS
MHT         = 0;      % offset with respect to zero level in model: assumption is that MHT = 0 m
maxvegeff   = 1.0;
repose_threshold = 0.3;  % vegetation threshold for applying repose_veg
gw           = round(eqtopo0 .* groundwater_depth);           % gw lies under beach with less steep angle

dune_foot_level_m = mean_sea_level + 3; % height above MSL in m
min_crest_level_m = mean_sea_level + 6; % height above MSL in m

%% STARTING CONDITIONS
topo         = topo0;          % initialise the topography map (slab units)
eqtopo       = eqtopo0;
spec1        = spec1at0;       % initialise effectiveness map for species 1
spec2        = spec2at0;       % initialise effectiveness map for species 2 => 0
spec3        = spec1.*0;       % initialise effectiveness map for species 3 => 0
balance      = topo*0;         % initialise the sedimentation balance map [slabs]
stability    = topo*0;         % initialise the stability map [slabs]
sp1_peak_at0 = sp1_peak;       % store initial peak growth of sp. 1
sp2_peak_at0 = sp2_peak;        % store initial peak growth of sp. 2

%balancecopy  = balance;
inundated    = zeros(size(topo));   % initial area of wave/current action
pbeachupdatecum = zeros(size(topo));
beachcount   = 1;
vegcount     = 1;
%year=0;

veg = spec1+spec2+spec3;            % determine the initial cumulative vegetation effectiveness
veg(veg>maxvegeff)=maxvegeff;                       % cumulative vegetation effectiveness cannot be negative or larger than one
veg(veg<0)=0;                       


%% INITIALISING MATRICES FOR BUDGET CALCULATIONS
iterations = iterations_per_cycle*simulation_time_y;   % number of iterations
timeits = (1:1:iterations);              % time vector for budget calculations
seainput_slabs = zeros(size(timeits));   % inititalise vector for sea-transported slabs

% inititalise vectors for transport activity
[windtransp_slabs, ...
    landward_transport, ...
    avalanches, ...
    crest_deposition, ...
    transport_across_foot] = deal(zeros(size(timeits))); 

% initialise vectors and matrices for yearly state variables
[V, F, C, Zmax, Zpos] = deal(nan(1,simy));
[vegs, topos] = deal(nan(crossshore,simy+1));

% initial dune volume
[v, f, z_max, z_pos] = get_dune_statistics(x, mean(topo0,1)*slabheight, dune_foot_level_m, min_crest_level_m);
F0 = f;
V0 = v;
Zmax0 = z_max;
Zpos0 = z_pos;
C0 = get_vegetation_cover(topo.*slabheight, veg, dune_foot_level_m);


topos(:,1) = mean(topo0,1)*slabheight;
vegs(:,1) = mean(veg,1);

% prepare plot window
if do_yearly_plot == 1
    h = figure('visible','on');
end

if do_yearly_plot == 1 && do_movie == 1
    h = figure('visible','Off');
    fn = ['./temp/scenario_blowouts_',num2str(simulation_time_y),'y_', num2str(profile_id),'_',num2str(area_code),'_SLR_',num2str(SLR, '%+03.1f'),'_VGR_',num2str(VGR, '%+04.0f'),'_', num2str(run_number),'.avi'];
    aviobj=VideoWriter(fn);
    aviobj.FrameRate = 5;
    open(aviobj);
end

% colors = jet(simy);

%% THE MAIN ITERATION LOOP 
% (1 loop = 1 iteration)
for i = 1:iterations     
    
    year = ceil(i/iterations_per_cycle);  
    if mod(i, iterations_per_cycle) == 1
         disp(['start of year: ', num2str(year), '; peak growth = ', num2str(sp1_peak), '; current sea level = ', num2str(sea_level_trend_m(beachcount))])
    end

    %% SAND TRANSPORT
    before        = topo;
    gw            = eqtopo .* groundwater_depth;    
    sandmap       = topo>sea_level_trend(beachcount);                     % returns T(one) for sandy cells
    shadowmap     = shadowzones2(topo, slabheight, shadow_angle);  % returns T(one) for in shadow
    erosmap       = erosprobs(veg, shadowmap, sandmap, topo, gw, p_erosion); % returns map of erosion probabilities
    deposmap      = depprobs(veg, shadowmap, sandmap, p_deposition_bare, p_deposition_veg); % returns map of deposition probabilities
    
    [changemap,slabtransp,~,~,~,inland_transport_i] = shiftslabs3(erosmap, deposmap, jumplength);  % returns map of height changes
    topo          = topo+changemap;                               % changes applied to the topography
 
    [topo, aval]  = enforceslopes3(topo, veg, slabheight, repose_bare, repose_vegetated, repose_threshold); % enforce angles of repose: avalanching
    
    balance       = balance+(topo - before);                            % update the sedimentation balance map
    stability     = stability + abs(topo - before);   
    
    % determine sand fluxes (in m3/m)
    % aeolian transport
    windtransp_slabs(i) = (slabtransp * slabheight_m * cellsize^2) / longshore; 
    % aeolian transport across landward border
    landward_transport(i) = (inland_transport_i * slabheight_m * cellsize^2) / longshore; 
    % avalanche activity
    avalanches(i) = aval; 
    
    % determine aeolian sand fluxes across foot and crest
    [crest_dep, cross_foot] = deal(0);
    for alongshore_position = 1:longshore
        [~, foot_position, ~, crest_position] = get_dune_statistics(x, topo(alongshore_position,:)*slabheight_m, dune_foot_level_m, min_crest_level_m);
        crest_dep = crest_dep + sum(changemap(alongshore_position,crest_position:end));
        cross_foot = cross_foot + sum(changemap(alongshore_position,foot_position:end));
    end
    
    % convert to m3/m
    crest_deposition(i) = (crest_dep * slabheight_m * cellsize^2) / longshore;
    transport_across_foot(i) = (cross_foot * slabheight_m * cellsize^2) / longshore; 
      

    %% BEACHUPDATE
    if ismember(i,beachreset)                % update the inundated part of the beach
                            
        % adjust equilibrium profile to current sea-level rise S:
        S = sea_level_trend_m(beachcount);
        
        % determine horizontal retreat of profile (R):
        R = (1/tan(nearshore_slope)) * S;
        
        % translate equilibrium profile according to S and R (landward and
        % upward migration scenario)
        displacement = - (R * eq_slope) + S;
        
        % keep shoreline position fixed and raise surface by S (nourishment
        % scenario)
%         displacement = S;

        % apply displacement
        eqtopo = eqtopo0 + displacement/slabheight ;
        eqtopo = round(eqtopo);

        % process beachupdate
        inundatedold      = inundated;        % make a copy for later use
        before1           = topo;             % copy of topo before it is changed

        % secondary parameters (not for calibration)
        m28f            = 0.0;  % resistance of vegetation: 1 = full, 0 = none. 
        pwavemaxf       = 1.0;  % maximum erosive strenght of waves (if >1: overrules vegetation)
        pwaveminf       = 1.0;  % in area with waves always potential for action
        shelterf        = 1.0;  % exposure of sheltered cells: 0 = no shelter, 1 = full shelter.
       
        [topo, inundated, pbeachupdate] = marine_processes(tide_levels(beachcount), sea_level_trend(beachcount), slabheight_m, cellsize, topo, eqtopo ,veg , ...
                dissipation_rate, wave_energy, m28f, pwavemaxf, pwaveminf, depth_limit, shelterf);
        
        topo = round(topo);
        seainput          = topo - before1;   % sand added to the beach by the sea 
       % seainput_slabs(i) = nansum(seainput(:)); % total # of slabs added in this update
        seainput_slabs(i) = sum(seainput(~isnan(seainput)));
        
        inundated         = inundated|inundatedold;          % combine updated area from individual loops
        pbeachupdatecum   = pbeachupdate + pbeachupdatecum;  % cumulative beachupdate probabilities
        
        [topo, ~]         = enforceslopes3(topo, veg, slabheight, repose_bare, repose_vegetated, repose_threshold); % enforce angles of repose again
        balance           = balance + (topo - before1);
        stability         = stability + abs(topo-before1);
        
        % update counter
        beachcount        = beachcount + 1;
    end
    
    
%     balance      = balance + (topo - before);
%     stability    = stability + abs(topo-before);
        
   % enforce slopes (represents slumping of steep cliff)
%    topo         = enforceslopes3(topo, veg, slabheight, repose_bare, repose_veg, repose_threshold); % enforce angles of repose again
    
  
    
    %% VEGETATION
    if mod(i,iterations_per_cycle) == 0
        
        veg_multiplier = (1+growth_reduction_timeseries(vegcount));
                           
        sp1_peak = sp1_peak_at0 * veg_multiplier;
        sp2_peak = sp2_peak_at0 * veg_multiplier;
        
        spec1_old = spec1;
        spec2_old = spec2;
        spec1     = growthfunction1_sens(spec1, balance.*slabheight_m, sp1_a, sp1_b, sp1_c, sp1_d, sp1_e, sp1_peak);
        spec2     = growthfunction2_sens(spec2, balance.*slabheight_m, sp2_a, sp2_b, sp2_d, sp1_e, sp2_peak);

        lateral1 = lateral_expansion(spec1_old, 1, lateral_probability * veg_multiplier);
        lateral2 = lateral_expansion(spec2_old, 1, lateral_probability * veg_multiplier);
        pioneer1 = establish_new_vegetation(topo.*slabheight_m, sea_level_trend_m(beachcount-1), pioneer_probability*veg_multiplier) .* (spec1_old <= 0);
        pioneer2 = establish_new_vegetation(topo.*slabheight_m, sea_level_trend_m(beachcount-1), pioneer_probability*veg_multiplier) .* (spec2_old <= 0); % .* (stability==0);

        % determine changes in vegetation cover 
        spec1_diff = spec1 - spec1_old;
        spec2_diff = spec2 - spec2_old;

        % split cover changes into into gain and loss
        spec1_growth = spec1_diff .* (spec1_diff > 0);
        spec1_loss = spec1_diff .* (spec1_diff < 0);

        spec2_growth  = spec2_diff .* (spec2_diff > 0);
        spec2_loss = spec2_diff .* (spec2_diff < 0);

        % only allow growth in adjacent or pioneer cells
%         spec1_change_allowed = spec1_growth .* (lateral1 | pioneer1);
        spec1_change_allowed = min(1-veg, spec1_growth) .* (lateral1 | pioneer1);
%         spec2_change_allowed = spec2_growth .* (lateral2 | pioneer2);
        spec2_change_allowed = min(1-veg, spec2_growth) .* (lateral2 | pioneer2);

        % re-assemble gain and loss and add to original vegetation cover
        spec1 = spec1_old + spec1_change_allowed + spec1_loss;
        spec2 = spec2_old + spec2_change_allowed + spec2_loss;
        
        pbeachupdatecum(pbeachupdatecum<0) = 0;
        pbeachupdatecum(pbeachupdatecum>1) = 1;
        
        
        % remove species where beach is reset
        %spec1(pbeachupdatecum > 0 & spec1 > 0) =  0.0;
        %spec2(pbeachupdatecum > 0 & spec2 > 0) =  0.0;
        spec1 = spec1 .* (1 - pbeachupdatecum);
        spec2 = spec2 .* (1 - pbeachupdatecum);
        
        % limit to geomorphological range
        spec1_geom          = spec1;     
        spec1_geom(spec1<0) = 0;
        spec1_geom(spec1>1) = 1;
        spec2_geom          = spec2;
        spec2_geom(spec2<0) = 0;
        spec2_geom(spec2>1) = 1;
        veg                 = spec1_geom + spec2_geom;      % update vegmap
        veg(veg>maxvegeff)  = maxvegeff;        % limit to effective range
        veg(veg<0)          = 0;     
       
%         veg = add_wakes(veg,wakelen);
%         veg(veg>maxvegeff)  = maxvegeff;        % limit to effective range
%         veg(veg<0)          = 0;     


        % update counter
        vegcount = vegcount + 1;
       
        %% CALCULATE STATE VARIABLES
        % store cross-shore profiles of topography and vegetation in matrix
        % for analysis
        topos(:,year+1) = mean(topo,1)*slabheight;
        vegs(:,year+1) = mean(spec1_geom+spec2_geom,1);
        z = mean(topo.*slabheight,1);

        % store dune-foot position (coastal indicator)
        [v, f, z_max, z_pos] = get_dune_statistics(x, z, dune_foot_level_m+S, min_crest_level_m+S);
        V(year) = v; 
        F(year) = f;
        Zmax(year) = z_max;
        Zpos(year) = z_pos;
        
        % fraction of cells above dune-foot level covered by vegetation
        c = get_vegetation_cover(topo.*slabheight, veg, dune_foot_level_m+S);
        C(year) = c;
        
        
        

        % reset maps for next year
        balance_copy        = balance;
        balance(:)          = 0;        % reset the balance map
        inundated(:)        = 0;        % reset part of beach with wave/current action
        pbeachupdatecum(:)  = 0;
        stability(:)        = 0;        % reset the balance map
        
        if ismember(year, PBT);
            % clearcut
%             spec1(1:end, 1:end) = 0;
%             spec2(1:end, 1:end) = 0;
            
            % blow outs
            spec1(10:20, 180:200) = 0;
            spec2(10:20, 180:200) = 0;
            spec1(10:20, 220:240) = 0;
            spec2(10:20, 180:200) = 0;
            spec1(10:20, 260:280) = 0;
            spec2(10:20, 180:200) = 0;
        end
        
        
        if do_yearly_plot == 1
%             figure(h);
            
            surf(1:crossshore, 1:longshore, topo.*slabheight, (spec1>0)*1+(spec2>0)*2); 
            zlim([0 30])               % elevation limit
            caxis([-0.5 3.5])          % colormap limits
            shading flat; axis equal;  % do not interpolate, 1:1:1 axis aspect
            title(['t = ', num2str(year)])
            cols = colormap(summer(4));% custom color map
            cols = cols(end:-1:1,:);
            colormap(cols)
            
            % add custom colorbar
            hcb = colorbar('YTickLabel',...
                            {'Bare','Pioneer','Stabiliser','Both'}, ...
                            'Location', 'EastOutside');
            set(hcb,'YTick',0:3)

            

            % fn = ['./temp/final_topography_',num2str(simulation_time_y),'y_profile_',num2str(profile_id),'_run_',num2str(run_number, '%2.2i'),'_SLR_',num2str(SLR, '%+.1f'),'VGR_',num2str(VGR, '%+.0f'),num2str(year,'%03d'),'.tif'];           
            % save_fig(h, fn, 6, 5, 300, 0)

            if do_movie == 1
                img = hardcopy(h, '-dzbuffer', '-r0');
                writeVideo(aviobj, im2frame(img));
            end
        end
    end
end
if do_endplot == 1
    try
        % endplots
        m = 3;
        n = 2;
        h=figure('visible', 'off');
        subplot(m,n,1); hold on
        set(gca,'ColorOrder',jet(size(topos,2)))
        plot(1:crossshore, topos); title(['profile ', num2str(profile_id), '/', num2str(area_code)]);
        ylim([0,25])
        xlim([0 size(topos,1)])
        xlabel('cross-shore distance (m)')
        ylabel('elevation (m)')
        subplot(m,n,2)
        veg_species = (spec1>0)*1 + (spec2>0)*2;
        surf(1:crossshore, 1:longshore, topo.*slabheight, veg_species); shading interp; axis equal;
        set(gca,'FontSize',3)
        shading interp; axis equal;
        cols = colormap(summer(10));
        cols = cols(end:-1:1,:);
        colormap(cols);
        caxis([0 3])
        subplot(m,n,[3 4])
        plot( (1:simulation_time_y) + start_year, V,'b-o')
        ylabel('dune volume (m^3/m)')
        xlim([start_year start_year+simulation_time_y]) 
        subplot(m,n,[5 6])
        plot( (1:simulation_time_y) + start_year, C,'b-o')
        xlim([start_year start_year+simulation_time_y]) 
        ylabel('vegetation cover')
        fn = ['./temp/pertubation_',num2str(simulation_time_y),'y_profile_',num2str(profile_id),'_run_',num2str(run_number, '%2.2i'),'_SLR_',num2str(SLR, '%+.1f'),'VGR_',num2str(VGR, '%+.0f'), '_at_t_',num2str(perturbation_time),'.tif'];
        save_fig(h, fn, 6, 5, 300, 0)
    catch ME
        disp(ME)
    end
end
% close(h)

%% MAKE FIGURES AND MOVIES
% endplots
OUT.configuration.param = {fieldnames(OPT)'};
OUT.configuration.value = get_values(OPT)';
OUT.topos   = topos;
OUT.vegs    = vegs;
OUT.V       = [V0 V];
OUT.F       = [F0 F];
OUT.C       = [C0 C];
OUT.Zmax    = [Zmax0 Zmax];
OUT.Zpos    = [Zpos0 Zpos];
OUT.spec1   = spec1;
OUT.spec2   = spec2;
OUT.topography = topo.*slabheight;
OUT.vegetation = veg;
OUT.landward_transport = landward_transport;
OUT.transport = windtransp_slabs;
OUT.crest_deposition = crest_deposition;
OUT.transport_across_foot = transport_across_foot;
if exist('M','var') && do_movie == 1
    close(aviobj)
end

