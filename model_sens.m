function [OUT] = model_sens(OPT)
% Dune development model
% includes sand transport, vegetation growth of 2 species and beachupdate
% Andreas Baas, modified by Alma de Groot, modified by Joep Keijsers
% 28/02/2014
do_yearly_plot = 0;
do_end_plot    = 1;
disp('Initializing ... ')

% Unpack input variables because we get tired of using OPT.<variable>
% disp(OPT)
fields = fieldnames(OPT, '-full');
numberOfFields = length(fields);

for f = 1 : numberOfFields
    thisField = fields{f};
    commandLine = sprintf('%s = OPT.%s', thisField, thisField);
    evalc(commandLine);
end

% parameters that need to be integer or rounded:
simy        = round(simulation_time_y);
vegc        = round(iterations_per_cycle);
slabh       = round(slabheight_m*100)/100;
% mean_sea_level         = round(mean_sea_level*10)/10;
hopl        = round(jumplength);
wakelength    = 0;

if (sp1_a<sp1_b<sp1_c<sp1_d<sp1_e && sp2_a<sp2_b<sp2_d<sp1_e) == 0
    error('Growth function error: need increasing x-values')
end

%% TIME AND SPACE SETTINGS & LOADING INITIAL MAPS
simyears = simy ;        % number of years simulated
% vegetationcycle = round(vegc/ (hopl/1) / (slabh/0.1) / (0.6/p_dep_sand));   % cycle for updating vegetation response: this determines how long a year is [iterations/year]
vegetationcycle = vegc;
beachreset   = round(0:(vegetationcycle/26):vegetationcycle*simy);        % how often to reset the wet beach part [iterarions] Should correspond to appr 2 weeks
time = 1:(simyears*vegetationcycle);

% topography settings 
% (already used for creating initial matrices to be loaded)
cellsize    = 1;                    % interpreted cell size [m]
slabheight  = slabh;                  % as a ratio of cell dimension
slabheightm = slabheight * cellsize;  % slab height in m

% read JARKUS profile from NetCDF file
[topoin, vegin] = read_profile_from_netcdf(area_code, profile_id, start_year, 0);

% create required topographies and initial vegetation map
[topo, eqtopo, spec1at0, spec2at0] = create_topographies(topoin, vegin);

% equilibrium topography
% load          eqtopo01x250;                  % get equilibrium beach (= part of topo0) [m]
eqtopo      = round(eqtopo./slabheightm);     % transform from m into number of slabs
topo0       = round(topo./slabheightm); 
waterlevels = read_waterlevels_from_netcdf(start_year,simy,26);

% x and y coordinates
crossshore = size(topo0, 2);    % cross-shore size of topography [m]
longshore  = size(topo0, 1);    % along-shore size of topography [m]
x = (cellsize:cellsize:ceil(crossshore)); % shore normal axis for plotting. 
y = (cellsize:cellsize:ceil(longshore));  % shore parallel axis for plotting


%% ADDITIONAL MODEL PARAMETERS
dune_foot_level_m   = 3.0;
min_crest_height_m  = 6.0;
maxvegeff           = 1.0;
gw                  = round(eqtopo .* groundwater_depth);           % gw lies under beach with less steep angle
repose_threshold    = 0.3;
MHT                 = mean_sea_level;      % offset with respect to zero level in model: assumption is that MHT = 0 m
waterlevels         = (waterlevels + MHT)./slabheightm;  % [m] to [slabs]

%% STARTING CONDITIONS
topo         = topo0;               % initialise the topography map (slab units)
spec1        = spec1at0;            % initialise effectiveness map for species 1
spec2        = spec2at0;            % initialise effectiveness map for species 2 => 0
spec3        = spec1.*0;            % initialise effectiveness map for species 3 => 0
balance      = topo*0;              % initialise the sedimentation balance map [slabs]
stability    = topo*0;              % initialise the stability map [slabs]
inundated    = zeros(size(topo));   % initial area of wave/current action
pbeachupdatecum = zeros(size(topo));
beachcount   = 0;

veg = spec1+spec2+spec3;            % determine the initial cumulative vegetation effectiveness
veg(veg>maxvegeff)=maxvegeff;       % cumulative vegetation effectiveness cannot be negative or larger than one
veg(veg<0)=0;                       


%% INITIALISING MATRICES FOR BUDGET CALCULATIONS
iterations = vegetationcycle*simyears;   % number of iterations
timeits = (1:1:iterations);              % time vector for budget calculations
seainput_slabs = zeros(size(timeits));   % inititalise vector for sea-transported slabs
windtransp_slabs = zeros(size(timeits)); % inititalise vector for wind-transported slabs

[vegs, topos, eros, deps] = deal(nan(crossshore,simy+1));

% beach-dune state indicators
[V, F, C, Zmax, Zpos] = deal(nan(1,iterations));
[v, f, z_max, z_pos] = get_dune_statistics(x, mean(topo0,1)*slabheight, dune_foot_level_m, min_crest_height_m);
F0 = f;
V0 = v;
Zmax0 = z_max;
Zpos0 = z_pos;
C0 = get_vegetation_cover(topo.*slabheight, veg, dune_foot_level_m);

% initial sprofiles
topos(:,1) = mean(topo0,1).*slabheight;
vegs(:,1) = mean(veg,1);

%% THE MAIN ITERATION LOOP 
% (1 loop = 1 iteration)
disp('Start iteration ...      ');

if do_yearly_plot == 1
    h = figure;
end
    
i = 1;
dt = 1;

while i<=iterations   
      
    year = floor(i/vegetationcycle);

    %% SAND TRANSPORT
    before        = topo;
    sandmap       = topo>0;                                       % returns T(one) for sandy cells
    shadowmap     = shadowzones2(topo, slabheight, shadow_angle);  % returns T(one) for in shadow
    erosmap       = erosprobs(veg, shadowmap, sandmap, topo, gw, p_erosion); % returns map of erosion probabilities
    deposmap      = depprobs(veg, shadowmap, sandmap, p_deposition_bare, p_deposition_veg); % returns map of deposition probabilities

    [changemap, slabtransp, ~, ero_profile, dep_profile] = shiftslabs3(erosmap, deposmap, jumplength);  % returns map of height changes  
    topo          = topo+changemap;                               % changes applied to the topography

    [topo, aval]  = enforceslopes3(topo, veg, slabheight, repose_bare, repose_vegetated, repose_threshold); % enforce angles of repose: avalanching
    
    windtransp_slabs(i) = slabtransp + aval;                      % transport by initial transport and avalanching together
    balance       = balance+(topo - before);                            % update the sedimentation balance map
    stability     = stability + abs(topo - before);
    
        
    %% BEACHUPDATE
    if ismember(i,beachreset)                % update the inundated part of the beach
        inundatedold      = inundated;        % make a copy for later use
        before1           = topo;             % copy of topo before it is changed
        beachcount        = beachcount + 1;
                
        % secondary parameters (not for calibration)
        m28f            = 0.0;  % resistance of vegetation: 1 = full, 0 = none. 
        pwavemaxf       = 1.0;  % maximum erosive strenght of waves (if >1: overrules vegetation)
        pwaveminf       = 1.0;  % in area with waves always potential for action
        shelterf        = 1.0;  % exposure of sheltered cells: 0 = no shelter, 1 = full shelter.
        
        % do not run beach update if waterlevel is below 0
        if waterlevels(beachcount) > 0

            [topo, inundated, pbeachupdate] = marine_processes(waterlevels(beachcount), MHT, slabheightm, cellsize, topo, eqtopo ,veg , ...
                dissipation_rate, wave_energy, m28f, pwavemaxf, pwaveminf, depth_limit, shelterf);

            % round output topography to whole slabs
            topo = round(topo);
            seainput          = topo - before1;   % sand added to the beach by the sea 
            seainput_slabs(i) = sum(seainput(~isnan(seainput)));

            inundated         = inundated|inundatedold;          % combine updated area from individual loops
            pbeachupdatecum   = pbeachupdate + pbeachupdatecum;  % cumulative beachupdate probabilities
        
%         figure(hh);    
%         surf(topo*slabheight); shading interp; axis equal;
%         title(['t = ', i/(simy*vegc)]);

            [topo, ~]         = enforceslopes3(topo, veg, slabheight, repose_bare, repose_vegetated, repose_threshold); % enforce angles of repose again
            balance           = balance + (topo - before1);
            stability         = stability + abs(topo-before1);
        end
  
    end
    

        
    %% UPDATE VEGETATION
    if mod(i,vegetationcycle) == 0
              
        spec1_old = spec1;
        spec2_old = spec2;
        spec1     = growthfunction1_sens(spec1, balance.*slabheightm, sp1_a, sp1_b, sp1_c, sp1_d, sp1_e, sp1_peak);
        spec2     = growthfunction2_sens(spec2, balance.*slabheightm, sp2_a, sp2_b, sp2_d, sp1_e, sp2_peak);

        lateral1 = lateral_expansion(spec1_old, 1, lateral_probability);
        lateral2 = lateral_expansion(spec2_old, 1, lateral_probability);
        pioneer1 = establish_new_vegetation(topo.*slabheightm, mean_sea_level, pioneer_probability) .* (spec1_old <= 0);
        pioneer2 = establish_new_vegetation(topo.*slabheightm, mean_sea_level, pioneer_probability) .* (spec2_old <= 0); % .* (stability == 0);

        % determine changes in vegetation cover 
        spec1_diff = spec1 - spec1_old;
        spec2_diff = spec2 - spec2_old;

        % split cover changes into into gain and loss
        spec1_growth = spec1_diff .* (spec1_diff > 0);
        spec1_loss = spec1_diff .* (spec1_diff < 0);

        spec2_growth  = spec2_diff .* (spec2_diff > 0);
        spec2_loss = spec2_diff .* (spec2_diff < 0);

        % only allow growth in adjacent or pioneer cells
%         spec1_change_allowed = spec1_growth .* (lateral1 | pioneer1);
        spec1_change_allowed = min(1-veg, spec1_growth) .* (lateral1 | pioneer1);
%         spec2_change_allowed = spec2_growth .* (lateral2 | pioneer2);
        spec2_change_allowed = min(1-veg, spec2_growth) .* (lateral2 | pioneer2);

        % re-assemble gain and loss and add to original vegetation cover
        spec1 = spec1_old + spec1_change_allowed + spec1_loss;
        spec2 = spec2_old + spec2_change_allowed + spec2_loss;

        pbeachupdatecum(pbeachupdatecum<0) = 0;
        pbeachupdatecum(pbeachupdatecum>1) = 1;
        
        % remove species where beach is reset
%         spec1(pbeachupdatecum > 0) =  0.0;
        spec1 = spec1 .* (1 - pbeachupdatecum);
%         spec2(pbeachupdatecum > 0) =  0.0;
        spec2 = spec2 .* (1 - pbeachupdatecum);
        
        % limit to geomorphological range
        spec1_geom          = spec1;     
        spec1_geom(spec1<0) = 0;
        spec1_geom(spec1>1) = 1;
        spec2_geom          = spec2;
        spec2_geom(spec2<0) = 0;
        spec2_geom(spec2>1) = 1;
        veg                 = spec1_geom + spec2_geom;
% %         
%         spec1          = spec1;     
%         spec1(spec1<0) = 0;
%         spec1(spec1>1) = 1;
%         spec2          = spec2;
%         spec2(spec2<0) = 0;
%         spec2(spec2>1) = 1;
%         veg                 = spec1 + spec2;
        
        % limit to effective range
        veg(veg>maxvegeff)  = maxvegeff;        
        veg(veg<0)          = 0;     
%     end
    

%         if (year+start_year) == 2005
%             spec1 = spec1 .* 0;
%             spec2 = spec2 .* 0;
%             veg = spec1 + spec2;
%         end
        %% store transect of topography and vegetation cover (long-shore average)
        topos(:,year+1) = mean(topo,1)*slabheight;
        vegs(:,year+1) = mean(veg,1);
        eros(:,year+1) = ero_profile;
        deps(:,year+1) = dep_profile;
        
%         veg = add_wakes(veg,wakelength);
%         veg(veg>maxvegeff)  = maxvegeff;        % limit to effective range
%         veg(veg<0)          = 0;     
        
       %% do yearly plot if requested
       if do_yearly_plot == 1
           figure(h)
           subplot(3,1,1)
           plot(mean(topo.*slabheight_m,1))
           hold on
           plot(mean(pbeachupdatecum,1))
           subplot(3,1,2)
           plot(mean(balance.*slabheight_m,1))
           hold on
           plot(xlim, [sp1_d sp1_d], 'r--')
           plot(xlim, [sp1_b sp1_b], 'r--')
           hold off
           subplot(3,1,3)
           plot(mean(spec1,1))
           %         save_fig(h, ['D:/Dropbox/model/fig/sims/c18_prof_',num2str(profile),'_2000_2010_',num2str(vegetationcycle),'_',num2str(mht),'.tif'], 6,6,300,0)
       end
       
        topos(:,year+1) = mean(topo,1)*slabheight;
        vegs(:,year+1) = mean(veg,1);
        
        % reset yearly maps
        balance(:)          = 0;        % reset the balance map
        stability_old       = stability;
        stability(:)        = 0;        % reset the balance map
        inundated(:)        = 0;        % reset part of beach with wave/current action
        pbeachupdatecum(:)  = 0;
       

       %% output progress to command window
        progress = round((i/iterations)*100);
        disp([num2str(round(progress)), '%']);
    end
    
    %% CALCULATE STATE VARIABLES
    % store cross-shore profiles of topography and vegetation in matrix
    % for analysis
    
    z = mean(topo.*slabheight,1);

    % store dune-foot position (coastal indicator)
    [v, f, z_max, z_pos] = get_dune_statistics(x, z, dune_foot_level_m, min_crest_height_m);
    V(i) = v; 
    F(i) = f;
    Zmax(i) = z_max;
    Zpos(i) = z_pos;

    % fraction of cells above dune-foot level covered by vegetation
    c = get_vegetation_cover(topo.*slabheight, veg, dune_foot_level_m);
    C(i) = c;
    
    i = i + dt;

end

% waitbar(0.95, w, 'writing output ...');
fprintf(1,'Writing output ...     ');
% measured volume (JARKUS)
% area=3;profile=17000;start_year=2002;simyears=9;
[z_obs, veg_obs] = read_profile_from_netcdf(area_code, profile_id, start_year, simulation_time_y);
% veg(:,end)
V_obs = nan(1,size(z_obs,2));
for i = 1:size(z_obs, 2)
    if ~all(isnan(z_obs(:,i)))
            V_obs(i) = get_dune_statistics(x, transp(z_obs(:,i)), dune_foot_level_m, min_crest_height_m);
    end
end

V_mod = [V0, V(vegetationcycle:vegetationcycle:simyears*vegetationcycle)];
ME_V       = calculate_model_efficiency(V_obs,V_mod, 'vol');

% changes in elevation (cross-shore profile)
dz_mod  = topos(:,size(topos,2)) - topos(:,1);
dz_obs = z_obs(:,size(z_obs,2)) - z_obs(:,1);

% downsample to original JARKUS resolution (1m to 5m)
dz_mod_5m = dz_mod(1:5:end);
dz_obs_5m = dz_obs(1:5:end);

% calculate model efficiency for elevation change (dz)
ME_dz       = calculate_model_efficiency(dz_obs_5m, dz_mod_5m, 'dz');

% create array of vegetation cover
veg_mod   = spec1_geom + spec2_geom;
veg_mod(veg_mod>1) = 1;
veg_mod(veg_mod<0) = 0;
veg_mod   = mean(veg_mod,1);

% create array of vegetation cover
veg_obs   = veg_obs(:,end);

% determine model efficiency pertaining to vegetation cover
ME_C = calculate_model_efficiency(veg_obs, veg_mod, 'veg');

%% MAKE FIGURES AND WRITE OUTPUT
if do_end_plot == 1
    endplots
    fn = './temp/no_stability_crit.tif';
    save_fig(gcf, fn, 10, 8, 600, 0)  
end

% model run settings
OUT.configuration.param = {fieldnames(OPT)'};
OUT.configuration.value = get_values(OPT)';
% yearly profiles of elevation and vegetation
OUT.topos   = topos;
OUT.vegs    = vegs;
% trends in dune characteristics
OUT.V       = [V0 V];
OUT.F       = [F0 F];
OUT.C       = [C0 C];
OUT.Zmax    = [Zmax0 Zmax];
OUT.Zpos    = [Zpos0 Zpos];
% final topography and vegetation cover
OUT.topography = topo.*slabheight;
OUT.spec1   = spec1_geom;
OUT.spec2   = spec2_geom;
OUT.vegetation = veg;
% Model Efficiency values
OUT.ME_V    = ME_V;
OUT.ME_dz   = ME_dz;
OUT.ME_C    = ME_C;
% store raw observation and simulation data used for calculating ME
OUT.veg_obs = veg_obs;
OUT.veg_mod = veg_mod;
OUT.dz_obs_5m = dz_obs_5m;
OUT.dz_mod_5m = dz_mod_5m;
OUT.V_obs   = V_obs;
OUT.V_mod   = V_mod;
OUT.longshore = longshore; 
OUT.crossshore= crossshore; 

fprintf(1,'Done. \n');
