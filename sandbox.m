%% sandbox
close all
clear all

% parameters
n_iterations = 50;
p_dep_sand = 0.06;
p_dep_bare = 0.4;
gw_reduction = 0.8;
jumplength=1;
wakelength=0;
slabheight=0.1;
shadowangle = 25;
cross_shore = 200;
long_shore = 100;

% initial maps
topo = zeros(long_shore,cross_shore) + 20;
gw = gw_reduction.*topo;

veg = zeros(long_shore,cross_shore);
% veg(1:50,80:85) = 0.2;
veg(1:50,90:95) = repmat(linspace(0,1,6),[50, 1]);
veg(50:100,100:120) = 0.4;
veg(:,120:140) = 0.6;

topos = nan(size(topo,2),n_iterations);
% veg = add_wakes(veg, wakelength);

for i = 1:n_iterations
    
    %% SAND TRANSPORT
    sandmap       = topo>0;                                       % returns T(one) for sandy cells
    shadowmap     = shadowzones2(topo, slabheight, shadowangle);  % returns T(one) for in shadow   
    erosmap       = erosprobs(veg, shadowmap, sandmap, topo, gw); 
    deposmap      = depprobs(veg, shadowmap, sandmap, p_dep_bare, p_dep_sand); 
    [changemap,slabtransp,pickedup] = shiftslabs3(erosmap, deposmap, jumplength);  
    topo          = topo+changemap;   
    [topo, aval]  = enforceslopes3(topo, veg, 0.1, 20, 40, 0.3); % enforce angles of repose: avalanching
    
        topos(:,i) = mean(topo,1);
    
    
end

figure; 
hold on

subplot(3,1,1)
% imagesc(1:cross,1:long,topo)
surf(1:cross_shore,1:long_shore,topo)
shading interp; axis equal;

subplot(3,1,2)
hold on
plot(mean(veg,1))
plot(mean(shadowmap,1),'r')
xlim([0 cross_shore])

subplot(3,1,3)
hold on
set(gca,'colororder',jet(size(topos,2)))
plot(topos)
plot(mean(topo,1),'k')
xlim([0 cross_shore])


