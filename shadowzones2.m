function r=shadowzones2(topof, sh, lee)
% returns a logical map with all shadowzones identified as ones
% wind from left to right, along the +2 dimension
% topof     : topography map [topo]
% sh        : slab height [slabheight]
% lee       : shadow angle [shadowangle]
% returns a logical map of shaded cells
% now with open boundaries

%% standalone test
% [topof, vegin] = read_profile_from_netcdf(10000,2002,9);
% topof = create_topographies(topof, vegin);
% sh = 0.1;
% lee = 5;

steplimit=ceil(tand(lee)/sh);   % the maximum step difference allowed 
                                 % given the slabheight and shadowangle
if steplimit<1
    steplimit=1;
end

% range=min(size(topof,2), max(topof(:))/steplimit);
range=nanmax(topof(:))/steplimit;  % identifies highest elevation and uses
                                % that to determine what the largest search
                                % distance needs to be
% avoid search distance larger than size of topography (causes error)
if range>size(topof,2)
    range=size(topof,2);
end

inshade=zeros(size(topof));     % define the zeroed logical map
for i=1:range
    step=topof-circshift(topof, [0,i]);     % shift across columns (2nd dimension; along a row)
    tempinshade = bsxfun(@lt, step, -floor(steplimit*i));    % identify cells with too great a stepheight
    tempinshade(:,1:i)=0;                   % part that is circshifted back into beginning of space is ignored
    inshade=inshade|tempinshade;            % merge with previous inshade zones
end

r=inshade;

